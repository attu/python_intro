HTML := $(SRCS:%.md=%.html)

build: $(HTML) python_training.md
	pandoc --standalone -t revealjs -o html/index.html python_training.md
