from invoke import task, Collection
from . import test
from . import style


@task
def build(ctx):
    ctx.run('pandoc --standalone -t revealjs -o html/index.html python_training.md')


@task(pre=[build])
def deploy(ctx, src='html', dest='public'):
    ctx.run(f'mkdir -p {dest}')
    ctx.run(f'cp -R {src} {dest}')


@task
def book(ctx):
    cmd = f'pandoc --pdf-engine=xelatex -o python_training.pdf python_training.md'
    ctx.run(cmd, hide='stderr')


@task(pre=[build, test.test, style.check, deploy])
def all(ctx):
    pass


ns = Collection(build, book, test.test, all, deploy)
ns.add_collection(Collection(style.check, style.format), name='style')
