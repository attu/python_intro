from invoke import task


@task
def test(ctx, verbose=False):
    ctx.run(f'pytest {"-v" if verbose else ""}')
