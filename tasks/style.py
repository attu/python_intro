from invoke import task
from glob import glob


@task
def check(ctx):
    for filename in glob('**/*.py', recursive=True):
        ctx.run(f'pycodestyle {filename}')


@task
def format(ctx):
    for filename in glob('**/*.py', recursive=True):
        ctx.run(f'autopep8 -iaa {filename}')
