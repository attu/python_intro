import zmq
import json
from random import randrange
from .console import console
from concurrent.futures import ThreadPoolExecutor as Executor


def candidates(first):
    while True:
        yield first
        first += 2


def request_handler(first):
    ctx = zmq.Context()
    socket = ctx.socket(zmq.REQ)
    args = console()
    socket.connect(f'tcp://{args.broker_host}:{args.broker_frontend_port}')

    while True:
        for c in candidates(first):
            socket.send(json.dumps(dict(value=c)).encode('utf-8'))
            socket.recv()


def main():
    jobs = [2**randrange(70, 110) + 1 for _ in range(6)]
    with Executor(max_workers=6) as pool:
        g = pool.map(request_handler, jobs)
        for f in g:
            f.result()
