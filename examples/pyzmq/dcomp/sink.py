import zmq
import json
from .console import console


def main():
    args = console()

    context = zmq.Context()

    socket = context.socket(zmq.PULL)
    socket.bind(f'tcp://*:{args.sink_port}')

    while True:
        msg = json.loads(socket.recv().decode('utf-8'))
        print(f'got prime from {msg["worker"]}: {msg["value"]}')
