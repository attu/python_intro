import argparse


def console():
    parser = argparse.ArgumentParser(description='broker')
    parser.add_argument(
        '-H',
        '--broker-host',
        type=str,
        default='localhost',
        help='broker host')
    parser.add_argument(
        '-f',
        '--broker-frontend-port',
        type=int,
        help='broker frontend port')
    parser.add_argument(
        '-b',
        '--broker-backend-port',
        type=int,
        help='broker backend port')
    parser.add_argument(
        '-S',
        '--sink-host',
        type=str,
        default='localhost',
        help='sink port')
    parser.add_argument('-s', '--sink-port', type=int, help='sink port')
    parser.add_argument(
        '-c',
        '--config',
        type=argparse.FileType(),
        help='config file')
    args = parser.parse_args()
    if args.config:
        data = args.config.read().strip().split('\n')
        conf = parser.parse_args(data)
        args = parser.parse_args(namespace=conf)
    return args
