#!/usr/bin/env python3
import zmq
import time
import json
from uuid import uuid4
from .console import console
from .isprime import check


def main():
    args = console()

    context = zmq.Context()
    socket = context.socket(zmq.REP)
    sink = context.socket(zmq.PUSH)
    sink.connect(f'tcp://{args.sink_host}:{args.sink_port}')
    socket.connect(f'tcp://{args.broker_host}:{args.broker_backend_port}')
    print(f'connected to {args.broker_host}:{args.broker_backend_port}')

    uid = str(uuid4())

    while True:
        msg = json.loads(socket.recv().decode('utf-8'))
        socket.send(b'OK')
        if check(msg['value']):
            msg['status'] = 'prime'
            msg['worker'] = uid
            sink.send(json.dumps(msg).encode('utf-8'))
