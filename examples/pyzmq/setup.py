#!/usr/bin/env python

from setuptools import setup, find_packages


setup(
    name='broker',
    description='broker example',
    author='Karol Wozniak',
    author_email='karol.wozniak@email.com',
    keywords='zmq broker',
    url='https://gitlab.com/attu/dcomp',
    version='0.1',
    packages=find_packages(),
    install_requires=['pyzmq>=18.0'],
    entry_points={
        'console_scripts': [
            'broker = dcomp.broker:main',
            'sink = dcomp.sink:main',
            'client = dcomp.client:main',
            'worker = dcomp.worker:main',
        ]
    }
)
