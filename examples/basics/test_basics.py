from conftest import lorem
from counter import most_common
from decorator import log_me
from collections import Counter
from enumerate import task as gen_task
from list_comprehension import lst_compr, make_expected
from rotate import rotate
from meta_typed import Foo
from matrix import Matrix
import pytest


def test_most_common():
    assert most_common(n=5, txt=lorem) == [' ', 'i', 'e', 't', 'o']
    cnt = Counter(lorem)
    assert [c for c, _ in cnt.most_common(n=5)] == [' ', 'i', 'e', 't', 'o']


def test_logging_decorator():
    txt = ['']

    def sink(x):
        txt[0] = x

    @log_me('info', sink=sink)
    def foo(x: int, exponent: int = 2):
        return x**exponent
    assert foo(3, exponent=3) == 27
    assert txt[0] == 'info foo(3, exponent=3) -> 27'


def test_zipped_partial_generator():
    g = gen_task('lorem')
    assert tuple(g('ipsum')) == ((0, 'l', 'i'), (1, 'o', 'p'),
                                 (2, 'r', 's'), (3, 'e', 'u'), (4, 'm', 'm'))


def test_list_comprehension():
    x = [[0, 1, 2], [0, 1, 2, 3, 4], [0, 1, 2, 3]]
    assert lst_compr(x) == [0, -1, 4, 0, -1, 4, -3]
    assert lst_compr(x) == list(make_expected(x))


def test_rotate():
    assert rotate(list(range(5)), 2) == [2, 3, 4, 0, 1]


def test_meta_static_type():
    f = Foo()
    f.x = 42
    assert f.x == 42
    with pytest.raises(AssertionError):
        f.x = 'lorem'
    with pytest.raises(AttributeError):
        f.y = 42


def test_matrix_addition():
    m = Matrix([[0, 1, 2], [3, 4, 5], [6, 7, 8]])
    n = Matrix([[23, 19, 17], [13, 11, 7], [5, 3, 2]])
    m += n
    assert m == Matrix([[23, 20, 19], [16, 15, 12], [11, 10, 10]])


def test_matrix_multiplication():
    m = Matrix([[1, 0, 2], [-1, 3, 1]])
    n = Matrix([[3, 1], [2, 1], [1, 0]])
    assert (m * n) == Matrix([[5, 1], [4, 2]])
