class Meta(type):

    def __new__(meta, name, bases, dct, **kwds):
        def setter(self, name, value):
            if name in kwds.keys():
                assert isinstance(
                    value, kwds[name]), f'expected type of {name!r} is {kwds[name]}, got {type(value)}'
            return object.__setattr__(self, name, value)
        dct['__slots__'] = list(kwds.keys())
        dct['__setattr__'] = setter

        return super(Meta, meta).__new__(meta, name, bases, dct)


class Foo(metaclass=Meta, x=int):
    pass


f = Foo()
f.x = 42
print(f'f.x == {f.x}')

try:
    f.x = 'lorem'
except AssertionError as e:
    print('error: ', e)

try:
    f.y = 42
except AttributeError as e:
    print('error: ', e)
