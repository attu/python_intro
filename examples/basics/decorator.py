from functools import wraps


def log_me(msg: str, sink=print):
    def decor(f):
        @wraps(f)
        def inner(*args, **kwds):
            ret = f(*args, **kwds)

            def arguments():
                for a in args:
                    yield f'{a}'
                for k, v in kwds.items():
                    yield f'{k}={v}'
            sink(f'{msg} {f.__name__}({", ".join(arguments())}) -> {ret!r}')
            return ret
        return inner
    return decor


@log_me('info')
def foo(x: int, exponent: int = 2):
    return x**exponent


foo(3, exponent=3)
