from collections import defaultdict, Counter


def most_common(n: int, txt: str):
    data = defaultdict(int)
    for c in txt:
        data[c] += 1
    return [c for c, _ in sorted(data.items(), key=lambda x: x[1], reverse=True)[:n]]


lorem = """
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi
ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
culpa qui officia deserunt mollit anim id est laborum.
""".strip()


print(most_common(5, lorem))
cnt = Counter(lorem)
print(cnt.most_common(5))
