def rotate(lst: list, offset: int):
    return lst[offset:] + lst[:offset]


lst = list(range(5))
print(rotate(lst, 2))
