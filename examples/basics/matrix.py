class Matrix:
    def __init__(self, values: list):
        self.table = values

    def __getitem__(self, key):
        return self.table[key]

    def __iter__(self):
        return iter(self.table)

    def nrows(self): return len(self.table)

    def ncols(self):
        assert self.nrows() > 0
        return len(self[0])

    def row(self, i: int):
        for x in self.table[i]:
            yield x

    def col(self, j: int):
        for row in self.table:
            yield row[j]

    def __eq__(self, other):
        return self.table == other.table

    def __iadd__(self, other):
        assert self.nrows() == other.nrows()
        assert self.ncols() == other.ncols()
        for i, row in enumerate(other.table):
            for j, col in enumerate(row):
                self[i][j] += col
        return self

    def __mul__(self, other):
        assert self.ncols() == other.nrows()
        result = [[0 for _ in range(self.nrows())]
                  for _ in range(other.ncols())]
        for i in range(self.nrows()):
            for j in range(other.ncols()):
                result[i][j] = sum(
                    map(lambda x: x[0] * x[1], zip(self.row(i), other.col(j))))
        return Matrix(result)
