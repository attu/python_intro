def task(x):
    def impl(y):
        for a, (b, c) in enumerate(zip(x, y)):
            yield a, b, c
    return impl


g = task('lorem')
print(tuple(g('ipsum')))
