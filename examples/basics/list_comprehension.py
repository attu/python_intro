def lst_compr(li: list):
    return [e * e if e % 2 == 0 else -e for nested in li if len(nested) <= 4 for e in nested]


def make_expected(lst: list):
    for sub in lst:
        if len(sub) < 5:
            for e in sub:
                if e % 2 == 0:
                    yield e * e
                else:
                    yield -e


x = [[0, 1, 2], [0, 1, 2, 3, 4], [0, 1, 2, 3]]
print(lst_compr(x))
print(list(make_expected(x)))
