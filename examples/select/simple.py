import select
import socket
import sys
from queue import Queue, Empty
import logging
logging.basicConfig(
    format='[%(process)d:%(threadName)s] $ %(message)s',
    level=logging.INFO)
log = logging.getLogger(__name__)

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.setblocking(0)
server.bind(('', 8888))
server.listen(5)

inputs = [server]
outputs = []
message_queues = {}

log.info('entering loop...')
while inputs:
    readable, writable, exceptional = select.select(
        inputs, outputs, inputs)

    for s in readable:
        if s is server:
            con, (host, port) = s.accept()
            log.info(f'connection from {host!r}:{port} accepted')
            con.setblocking(0)
            inputs.append(con)
            message_queues[con] = Queue()
        else:
            data = s.recv(1024)
            if data:
                host, port = s.getpeername()
                log.info(f'received {data!r} from {host!r}:{port}')
                message_queues[s].put(data)
                if s not in outputs:
                    outputs.append(s)
            else:
                if s in outputs:
                    outputs.remove(s)
                inputs.remove(s)
                s.close()
                del message_queues[s]
    for s in writable:
        try:
            msg = message_queues[s].get_nowait()
        except Empty:
            outputs.remove(s)
        else:
            host, port = s.getpeername()
            log.info(f'sending message {msg!r} to {host!r}:{port}')
            s.send(msg)
    for s in exceptional:
        inputs.remove(s)
        if s in outputs:
            outputs.remove(s)
        s.close()
        del message_queues[s]
