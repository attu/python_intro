from pytest import fixture
import psutil
from pathlib import Path
import signal
import os
import errno
from functools import wraps
from psutil import Process
import time
from logging import basicConfig, getLogger

basicConfig(format='%(asctime)s %(levelname)s %(filename)s#%(lineno)d $ %(message)s')
log = getLogger(name=__name__)


lorem = """
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi
ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
culpa qui officia deserunt mollit anim id est laborum.
""".strip()


class TimeoutError(Exception):
    pass


def timeout(seconds=0, error_message=os.strerror(errno.ETIME)):
    def decorator(func):
        def _handle_timeout(signum, frame):
            raise TimeoutError(error_message)

        @wraps(func)
        def wrapper(*args, **kwargs):
            signal.signal(signal.SIGALRM, _handle_timeout)
            signal.alarm(seconds)
            try:
                result = func(*args, **kwargs)
            finally:
                signal.alarm(0)
            return result
        return wrapper
    return decorator


def process(cmd: str, env: dict = None, timeout: int = None):
    env = env or os.environ.copy()
    log.debug(f'starting process {cmd!r}')
    process = psutil.Popen(cmd.strip().split(), env=env)
    try:
        yield process
    finally:
        log.info(f'terminating process {process!r}')
        to_kill = [process]
        to_kill.extend(process.children(recursive=True))
        for p in to_kill:
            process.terminate()
        _, alive = psutil.wait_procs(to_kill, timeout=1)
        for p in alive:
            log.error(f'process {process!r} hangs, sending SIGKILL')
            p.kill()
        else:
            # assert False
            pass


def script(name: str):
    return next(Path().glob(name))


def server(cmd: str):
    @timeout(1)
    def wait(p: Process):
        while not p.connections():
            pass
        return p

    for p in process(cmd, timeout=1):
        yield wait(p)


@fixture
def stream_server():
    for p in server(f'python3 {script("**/asyncio/streams.py")}'):
        yield p


@fixture
def blocking_process_server():
    for p in server(f'python {script("**/asyncio/blocking.py")} process'):
        yield p


@fixture
def blocking_thread_server():
    for p in server(f'python {script("**/asyncio/blocking.py")} thread'):
        yield p
