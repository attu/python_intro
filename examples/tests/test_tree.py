import unittest
from tree import tree, walk


class TreeTest(unittest.TestCase):

    def setUp(self):
        self.tree = tree()

    def test_sizeOfEmptyTreeIsZero(self):
        assert len(self.tree) == 0

    def test_EmptyTreeConvertsToFalse(self):
        assert not bool(self.tree)

    def test_canCreateNestedNodes(self):
        self.tree['outerNode']['nestedNode'] = 'lorem'
        assert 'outerNode' in self.tree
        assert 'nestedNode' in self.tree['outerNode']
        assert self.tree['outerNode']['nestedNode'] == 'lorem'

    def test_canWalkTheTreeNodes(self):
        self.tree['lorem']['ipsum'] = 42
        self.tree['lorem']['dolor'] = 'sit'
        self.tree['amet']['consectetur'] = 3.14
        self.tree['amet']['adipiscing'] = (2.71, 6.62)
        assert {2.71, 6.62, 3.14, 'sit', 42} == set(walk(self.tree))


if __name__ == '__main__':
    unittest.main()
