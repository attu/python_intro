from collections import defaultdict


def tree(): return defaultdict(tree)


def walk(t):
    if isinstance(t, dict):
        for k, v in t.items():
            yield from walk(v)
    elif isinstance(t, (list, tuple)):
        for v in t:
            yield from walk(v)
    else:
        yield t
