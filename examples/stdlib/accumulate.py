from itertools import accumulate

arr = [15, -2, 2, -8, 1, 7, 10, 23]

sub = dict()
ret = (0, (0, 0))
for i, v in enumerate(accumulate(arr), start=1):
    if v not in sub:
        sub[v] = i
    else:
        ret = max((i - sub[v], (sub[v], i)), ret)

l, (x, y) = ret
print(f'largest subarray with lenght {l} that sum equals to 0 is {arr[x:y]}')
