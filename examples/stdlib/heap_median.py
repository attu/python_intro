import heapq


class heap(object):
    def __init__(self, func=lambda x: x):
        self.storage = []
        self.func = func

    def push(self, value):
        return heapq.heappush(self.storage, self.func(value))

    def pop(self):
        return self.func(heapq.heappop(self.storage))

    def __getitem__(self, i):
        return self.func(self.storage[i])

    def __len__(self):
        return len(self.storage)


minheap = heap


def maxheap(): return heap(func=lambda x: -x)


def median():
    def rebalance(x, y):
        if abs(len(x) - len(y)) == 2:
            if len(x) > len(y):
                y.push(x.pop())
            else:
                x.push(y.pop())

    def calc(x, y):
        if len(x) == len(y):
            return (x[0] + y[0]) / 2
        if len(x) > len(y):
            return x[0]
        return y[0]
    highs = minheap()
    lows = maxheap()
    x = yield
    lows.push(x)
    while True:
        y = yield float(x)
        if y >= lows[0]:
            highs.push(y)
        else:
            lows.push(y)
        rebalance(lows, highs)
        x = calc(lows, highs)


def data_stream():
    data = [3, 5, 7, 9, 1, ]
    for x in data:
        yield x


data = data_stream()

m = median()
next(m)

print(m.send(int(next(data))))
print(m.send(int(next(data))))
print(m.send(int(next(data))))
print(m.send(int(next(data))))
print(m.send(int(next(data))))
