import threading
import time
import logging

logging.basicConfig(
    format='[%(threadName)s] $ %(message)s', level=logging.INFO)
log = logging.getLogger(__name__)


def job():
    log.info('starting job')
    time.sleep(3)
    log.info('job done')


log.info('creating thread')
t = threading.Thread(target=job)
log.info(f'starting thread {t!r}')
t.start()
t.join()
log.info(f'joined {t!r}')
