from threading import Thread, Condition
import logging
import time

logging.basicConfig(
    format='[%(process)d:%(threadName)s] $ %(message)s', level=logging.INFO)
log = logging.getLogger(__name__)


def consumer(cond: Condition):
    log.info('starting consumer')
    with cond:
        cond.wait()
        log.info('resource is available')
        cond.notify(n=1)
        log.info('consumer notify')


def producer(cond: Condition):
    log.info('starting producer')
    with cond:
        log.info('creating resource')
        cond.notify(n=1)


cond = Condition()
cons = [Thread(target=consumer, args=(cond,)) for _ in range(2)]
prod = Thread(target=producer, args=(cond,))

for c in cons:
    c.start()
    time.sleep(0.1)
prod.start()
