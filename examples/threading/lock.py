from threading import Thread, Lock
import logging
import time
from random import randrange

logging.basicConfig(
    format='[%(process)d:%(threadName)s] $ %(message)s', level=logging.INFO)
log = logging.getLogger(__name__)


class Counter:

    def __init__(self):
        self.lock = Lock()
        self.value = 0


def worker(cnt: Counter):
    log.info('waiting for lock')
    time.sleep(randrange(1, 3))
    with cnt.lock:
        log.info(f'lock acquired, value: {cnt.value}')
        cnt.value += 1


cnt = Counter()
for i in range(4):
    t = Thread(target=worker, args=(cnt,))
    t.start()
