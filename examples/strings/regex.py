import re

sample = """
Ernst Stavro Blofeld
Leader, Number 1
SPECTRE
12 Rue des Mechants
75000 Paris, France
+33 1 12 34 56 78
number1@spectre.org
twitter: www.twitter.org/ernieblo
""".strip()

pattern = r"""
(?P<name>\w+ \w+)\s+(?P<surname>\w+)
(?P<title>.*)
(?P<company>.*)
(?P<street_number>\d+)\s+(?P<street_name>.*)
(?P<postal_code>\d+)\s+(?P<city>\w+)\W*(?P<country>\w+)
(?P<phone>\+[\d ]+)
(?P<email>\S+)
""".strip()
