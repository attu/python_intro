from to_roman import to_roman
from regex import pattern, sample
import re


def test_integer_to_roman_notation_coversion():
    assert to_roman(1) == 'I'
    assert to_roman(2) == 'II'
    assert to_roman(3) == 'III'
    assert to_roman(4) == 'IV'
    assert to_roman(5) == 'V'
    assert to_roman(6) == 'VI'
    assert to_roman(7) == 'VII'
    assert to_roman(8) == 'VIII'
    assert to_roman(9) == 'IX'
    assert to_roman(10) == 'X'
    assert to_roman(40) == 'XL'
    assert to_roman(42) == 'XLII'
    assert to_roman(48) == 'XLVIII'
    assert to_roman(50) == 'L'
    assert to_roman(88) == 'LXXXVIII'
    assert to_roman(99) == 'XCIX'
    assert to_roman(101) == 'CI'
    assert to_roman(155) == 'CLV'
    assert to_roman(995) == 'CMXCV'
    assert to_roman(1986) == 'MCMLXXXVI'
    assert to_roman(2019) == 'MMXIX'


def test_foot_note_regex_pattern():
    result = re.match(pattern=pattern, string=sample, flags=re.MULTILINE)
    assert result
    assert result.groupdict() == {'city': 'Paris',
                                  'company': 'SPECTRE',
                                  'country': 'France',
                                  'email': 'number1@spectre.org',
                                  'name': 'Ernst Stavro',
                                  'phone': '+33 1 12 34 56 78',
                                  'postal_code': '75000',
                                  'street_name': 'Rue des Mechants',
                                  'street_number': '12',
                                  'surname': 'Blofeld',
                                  'title': 'Leader, Number 1'}
