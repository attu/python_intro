from psutil import Popen
from socket import socket
from threading import Thread, Barrier
from conftest import lorem, timeout
from contextlib import contextmanager


def chunks(source: list, sizes: list):
    it = iter(source)
    for s in sizes:
        yield list((next(it) for _ in range(s)))


@contextmanager
def exit(address: tuple, message: bytes = b'exit'):
    sock = socket()
    sock.connect(address)
    yield sock
    sock.sendall(message)
    sock.close()


def sender(bar: Barrier, address: tuple, messages: list):
    with exit(address) as sock:
        bar.wait(timeout=1)
        for msg in messages:
            sock.sendall(msg.encode('utf-8'))
            resp = sock.recv(1024)
            assert resp in {b'', b'done'}


def message_producer(address, msgs):
    bar = Barrier(len(msgs))
    threads = [Thread(target=sender, args=(bar, address, m))
               for m in chunks(lorem.replace(',', '').lower().split(), msgs)]

    for thr in threads:
        thr.start()
    for thr in threads:
        thr.join(timeout=1)


def test_streaming_server(stream_server: Popen):
    address = stream_server.connections()[0].laddr
    msgs = [2, 1, 1]
    message_producer(address, msgs)


def test_blocking_thread_server(blocking_thread_server: Popen):
    address = blocking_thread_server.connections()[0].laddr
    msgs = [2, 1, 1]
    message_producer(address, msgs)


def test_blocking_process_server(blocking_process_server: Popen):
    address = blocking_process_server.connections()[0].laddr
    msgs = [2, 1, 1]
    message_producer(address, msgs)
