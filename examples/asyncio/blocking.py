import asyncio
from asyncio import StreamReader, StreamWriter
import logging
from contextlib import suppress
from random import uniform
import time
import sys

if sys.argv[1] == 'process':
    from concurrent.futures import ProcessPoolExecutor as Executor

if sys.argv[1] == 'thread':
    from concurrent.futures import ThreadPoolExecutor as Executor

logging.basicConfig(
    format='[%(process)d:%(threadName)s] $ %(message)s', level=logging.INFO)
log = logging.getLogger(__name__)


def connection_handler(pool: Executor):
    async def handle_connection(reader: StreamReader,
                                writer: StreamWriter):
        host, port = writer.get_extra_info('peername')
        log.info(f'connecting {host!r}:{port}')
        while True:
            message = await reader.read(1024)
            if not message or message == b'exit':
                break
            message = message.decode().strip()
            ret = await handle_message(pool, message, writer, host, port)
            await writer.drain()
        log.info(f'{host!r}:{port} connection closed')
        writer.close()
    return handle_connection


def blocking(i: int, msg: str):
    log.info(f'I am blocking {msg!r}')
    time.sleep(uniform(0.5, 1.5))
    log.info(f'{msg!r} done...')


async def handle_message(pool: Executor,
                         message: str,
                         writer: StreamWriter,
                         host: str = 'localhost',
                         port: int = 0):
    log.info(f'{host!r}:{port} sends {message!r}')
    asyncio.create_task(job(message, writer))
    loop = asyncio.get_event_loop()
    blocking_tasks = [loop.run_in_executor(
        pool, blocking, i, message) for i in range(3, 5)]
    completed, pending = await asyncio.wait(blocking_tasks)
    log.info(f'message {message!r} handled')


async def job(message: str, writer: StreamWriter):
    log.info(f'starting task {message!r}')
    await asyncio.sleep(uniform(0.1, 0.5))
    log.info(f'task {message!r} finished')
    writer.write(b'done')


async def main(pool: Executor):
    server = await asyncio.start_server(
        connection_handler(pool), '0.0.0.0')
    host, port = server.sockets[0].getsockname()
    log.info(f'stream server starts on address {host!r}:{port}')
    async with server:
        await server.serve_forever()
    await asyncio.gather(*asyncio.all_tasks())


with suppress(KeyboardInterrupt), Executor(max_workers=3) as pool:
    asyncio.run(main(pool))
