import asyncio
import logging

logging.basicConfig(
    format='[%(threadName)s] $ %(message)s', level=logging.INFO)
log = logging.getLogger(__name__)


async def outer():
    log.info(f'calling coro_1')
    a = await coro_1()
    log.info(f'calling coro_2')
    b = await coro_2(a)
    log.info(f'returning outer')
    return a, b


async def coro_1():
    log.info(f'coro_1 called')
    return 42


async def coro_2(x):
    log.info(f'coro_2({x}) called')
    return 'lorem'

loop = asyncio.get_event_loop()

try:
    result = loop.run_until_complete(outer())
    log.info(f'outer returned {result}')
finally:
    loop.close()
