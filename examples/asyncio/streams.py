import asyncio
from asyncio import StreamReader, StreamWriter
import logging
from contextlib import suppress
from random import uniform


logging.basicConfig(
    format='[%(process)d:%(threadName)s] $ %(message)s', level=logging.INFO)
log = logging.getLogger(__name__)


async def handle_connection(reader: StreamReader,
                            writer: StreamWriter):
    host, port = writer.get_extra_info('peername')
    log.info(f'connecting {host!r}:{port}')
    while True:
        message = await reader.read(1024)
        if not message or message == b'exit':
            break
        message = message.decode().strip()
        ret = await handle_message(message, writer, host, port)
        await writer.drain()
    log.info(f'{host!r}:{port} connection closed')
    writer.close()


async def handle_message(message: str,
                         writer: StreamWriter,
                         host: str = 'localhost',
                         port: int = 0):
    log.info(f'{host!r}:{port} sends {message!r}')
    asyncio.create_task(job(message, writer))
    log.info(f'message {message!r} handled')


async def job(message: str, writer: StreamWriter):
    log.info(f'starting task {message!r}')
    await asyncio.sleep(uniform(0.1, 0.5))
    log.info(f'task {message!r} finished')
    writer.write(b'done')


async def main():
    server = await asyncio.start_server(
        handle_connection, '0.0.0.0')
    host, port = server.sockets[0].getsockname()
    log.info(f'stream server starts on address {host!r}:{port}')
    async with server:
        await server.serve_forever()
    await asyncio.gather(*asyncio.all_tasks())


with suppress(KeyboardInterrupt):
    asyncio.run(main())
