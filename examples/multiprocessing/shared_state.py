from multiprocessing import Process, Manager, Event
import logging

logging.basicConfig(
    format='[%(process)d:%(threadName)s] $ %(message)s', level=logging.INFO)
log = logging.getLogger()


def producer(ns, event):
    ns.value = 'lorem'
    log.info(f'namespace id#{id(ns)} value set to {ns.value!r}')
    event.set()


def consumer(ns, event):
    log.info('awaiting event')
    event.wait()
    log.info(f'got event, ns id#{id(ns)} value is {ns.value!r}')


mgr = Manager()
namespace = mgr.Namespace()
event = Event()
p = Process(target=producer, args=(namespace, event))
c = Process(target=consumer, args=(namespace, event))

c.start()
p.start()

c.join()
p.join()
