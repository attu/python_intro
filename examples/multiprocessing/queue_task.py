from multiprocessing import JoinableQueue, Queue, Process
import time
import random
import logging

logging.basicConfig(
    format='[%(process)d:%(threadName)s] $ %(message)s', level=logging.INFO)
log = logging.getLogger()


class Consumer(Process):

    def __init__(self, tasks: JoinableQueue, results: Queue):
        Process.__init__(self)
        self.tasks = tasks
        self.results = results

    def run(self):
        while True:
            next_task = self.tasks.get()
            if next_task is None:
                log.info(f'{self.name}: Exiting')
                self.tasks.task_done()
                break
            log.info(f'{self.name} is doing task')
            answer = next_task()
            self.tasks.task_done()
            self.results.put(answer)


def job():
    time.sleep(random.randrange(1, 3))
    return random.randrange(10, 20)


tasks = JoinableQueue()
results = Queue()

consumers = [Consumer(tasks, results) for i in range(3)]

for w in consumers:
    w.start()

num_jobs = 6
for i in range(num_jobs):
    tasks.put(job)

for c in consumers:
    tasks.put(None)

tasks.join()

for _ in range(num_jobs):
    log.info(f'Result is {results.get()}')
