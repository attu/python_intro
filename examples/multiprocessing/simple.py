import multiprocessing
import time
import logging

logging.basicConfig(
    format='[%(process)d:%(threadName)s] $ %(message)s', level=logging.INFO)
log = logging.getLogger(__name__)


def job():
    log.info('starting job')
    time.sleep(3)
    log.info('job done')


log.info('creating process')
p = multiprocessing.Process(target=job)
log.info(f'starting process {p!r}')
p.start()
p.join()
log.info(f'joined {p!r}')
