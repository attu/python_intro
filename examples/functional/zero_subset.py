from itertools import accumulate


def largest_zero_subarray(lst: list):
    sub = dict()
    ret = (0, (0, 0))
    for i, v in enumerate(accumulate(lst), start=1):
        if v not in sub:
            sub[v] = i
        else:
            ret = max((i - sub[v], (sub[v], i)), ret)
    return ret
