from zero_subset import largest_zero_subarray


def test_finding_largest_zero_subarray():
    array = [15, -2, 2, -8, 1, 7, 10, 23]
    lenght, (first, last) = largest_zero_subarray(array)
    assert lenght == 5
    assert array[first:last] == [-2, 2, -8, 1, 7]
