---
author: Karol Wozniak
title: 'Python training'
revealjs-url: 'reveal.js'
theme: solarized
---

## About me

Karol Wozniak

<karol.wozniak@email.com>

<https://attu.gitlab.io/python_intro>

# Agenda

## part 1

* overview
* language basics
* packaging
* unittest
* functional

## part 2

* strings
* serialization
* operating system
* stdlib
* networking
* concurrency

# Overview

## what is python?

<https://www.python.org>

## what is python?

high-level general purpose programming language

## what is python?

first release 1991, Guido van Rossum

## what is python?

targets readability and minimal boilerplate

::: notes
performance vs efficiency
power-driven optimizations, resource utilization
low latency computing
development performance
:::

## zen of python

<small>

```text
>>> import this                                                                 
The Zen of Python, by Tim Peters

Beautiful is better than ugly.
Explicit is better than implicit.
Simple is better than complex.
Complex is better than complicated.
Flat is better than nested.
Sparse is better than dense.
Readability counts.
Special cases aren't special enough to break the rules.
Although practicality beats purity.
Errors should never pass silently.
Unless explicitly silenced.
In the face of ambiguity, refuse the temptation to guess.
There should be one-- and preferably only one --obvious way to do it.
Although that way may not be obvious at first unless you're Dutch.
Now is better than never.
Although never is often better than *right* now.
If the implementation is hard to explain, it's a bad idea.
If the implementation is easy to explain, it may be a good idea.
Namespaces are one honking great idea -- let's do more of thos
```

</small>

## what is python?

interpreted scripts

::: notes
bytecode compiled into pyc files
:::

## what is python?

- dynamic typing
- strong typing
- duck typing

## what is python?

- object oriented
- structured
- functional

## reference implementation

[CPython](https://github.com/python/cpython)

::: notes
portability
:::

## other implementations

- [PyPy](https://pypy.org/)
- [Jython](https://www.jython.org/)
- [IronPython](https://ironpython.net/)

::: notes
pypy is using JIT compiler to produce more efficient code
:::

## bindings

- [CApi](https://docs.python.org/3.7/c-api/index.html)
- [Cython](https://cython.org/)
- [Boost.Python](https://www.boost.org/doc/libs/1_70_0/libs/python/doc/html/index.html)

## hello, world!

```python
print('hello, world!')
```

[wandbox](https://wandbox.org/permlink/4kvJllOWSSrdULb5)

## how to run

interactive

```python
$ python3
Python 3.7.3 (default, Jun 24 2019, 04:54:02)
[GCC 9.1.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> print('hello, world!')
hello, world!
```

## how to run

script

```
$ cat<<EOF > script.py
> print('hello, world!')
> EOF
$ python3 ./script.py
hello, world!
```

## python2 vs python3

python 2.0 released 2000, 2.7 EOL 2015 -> 2020

python 3.0 released 2008, current 3.7

::: notes
Incompatible, but backporting was done in some cases Large python2 codebase
:::

# Language basics

## scope

Same scope statements and expressions are defined by indentation level

## comments

```python
# this is single comment
"""
this is
multi line
comment
"""
```

## statements

```python
a,b = 'lorem', 42

foo(a, b, ipsum=3.14)

print('lorem', 42)
```

## if/elif/else

```python
if x == 'lorem' or 'ipsum' in x:
  print(f'x equals {x}')
elif x is None:
  print('x is None')
else:
  print('unsupported x')
```

## for loop

```python
for i in range(10):
  for c in 'lorem':
    print(c*i)
```

## for else

```python
stock = [3.14, 'lorem']
items = ['lorem', 42, 3.14]
found = []
for item in items:
  for element in stock:
    if item == element:
      found.append(item)
      break
  else:
    print(item, "not found!")
print('found', found)
```

## while loop

```python
>>> while(True):
...   x = input('enter value: ')
...   if x == '42':
...       print('correct!')
...       break
...   print('try again')
```

## pass/continue

```python
while True:
  pass
```
```python
for i in range(10):
  continue
```

## enumerate

```python
for idx, c in enumerate('lorem'):
  print(idx, c)
```

## enumerate

```text
0 l
1 o
2 r
3 e
4 m
```

## zip

```python
for x, y in zip('lorem', 'ipsum'):
  print(x, y)
```

## zip

```text
l i
o p
r s
e u
m m
```

## function definition

```python
def aqq():
  """
  does nothing
  """
  pass
```

## docstring

```text
>>> help(aqq)
Help on function aqq in module __main__:

aqq()
    does nothing
(END)
```

## function definition

```python
def bqq(x, y, z):
  return zip(x, y, z)
```

## function definition

```python
def bqq(*args):
  return zip(*args)
```

## functions are first class objects

```python
bqq = zip
```

## named parameters

```python
def cqq(*args, **kwds):
  print(args, kwds)

cqq(7, a='lorem')
```

## default parameters

```python
def dqq(x, y=[]):
  y.append(x)
  return y

print(dqq(7))
print(dqq(42))
```

## default parameters: gotcha

```text
[7]
[7, 42]
```

::: notes
Gotcha - defaults are created once, so it behaves like a function scope static in cpp
:::

## multiple return values

```python
def eqq(num, den = 1):
  return (num // den), (num % den)

div, mod = eqq(7, 3)
```

::: notes
c++17 fixes it with structure bindings for types with tuple protocol
:::

## yield

```python
def odds(x):
  x = x if x % 2 != 0 else x + 1
  while True:
    yield x
    x += 2

gen = odds(4)
print([next(gen) for _ in range(5)])
```

::: notes
Similar design to GoF Iterator pattern or c++ coroutines
:::

## nested functions

```python
def foo(x):
  def bar(y):
    return x + y
  return bar

print(foo(3)(7))
```
```
10
```

## TASK

Write a function that takes a string and returns a generator
taking string and yields enumerated zip of both

```python
def task(x):
  """
  implement me
  """

g = task('lorem')
print(tuple(g('ipsum')))
```
```
((0, 'l', 'i'), (1, 'o', 'p'), (2, 'r', 's'), (3, 'e', 'u'), (4, 'm', 'm'))
```

::: notes
[solution](https://gitlab.com/attu/python_intro/blob/master/examples/basics/enumerate.py)
:::

## core types: numbers

```python
42, -7, 3.14, 1+2j, 0b1011
```

## core types: strings

```python
'lorem', "ipsum", u"dolor"
```

::: notes
Multiline comment is also a string.
In python2 byte == str. In python3 it is separate type.
string is a rich type, there will be a own chapter for string
:::

## core types: list

```python
lst = [42, -7, 3.14, 'lorem', [0], []]
lst[0] = list('ipsum')
```

## core types: list

<small>

| function | description |
|--|--|
| L.append(object) -> None | append object to end |
| L.count(value) -> integer | return number of occurrences of value |
| L.extend(iterable) -> None | extend list by appending elements from the iterable |
| L.index(value, [start, [stop]]) -> integer | return first index of value |
| L.insert(index, object) | insert object before index |

</small>

::: notes
It is comparable to std::vector<PyObject*>.
There is not restriction on contained types.
It is mutable.
:::

## core types: list

<small>

| function | description |
|--|--|
| L.pop([index]) -> item | remove and return item at index (default last) |
| L.remove(value) -> None | remove first occurrence of value |
| L.reverse() -> None | reverse *IN PLACE* |
| L.sort(key=None, reverse=False) -> None | stable sort *IN PLACE* |

</small>

## core types: list-based queue

```python
def neighbours(graph, v):
    for u in graph[v]:
        yield u

def bfs(graph, start, func=lambda g, v: None):
    visited, items = set(), [start]
    while items:
        vertex = items.pop(0)
        if vertex not in visited:
            visited.add(vertex)
            func(graph, vertex)
            items.extend(neighbours(graph, vertex))
    return visited
```

## core types: list-based stack

```python
def neighbours(graph, v):
    for u in graph[v]:
        yield u

def dfs(graph, start, func=lambda g, v: None):
    visited, items = set(), [start]
    while items:
        vertex = items.pop(-1)
        if vertex not in visited:
            visited.add(vertex)
            func(graph, vertex)
            items.extend(neighbours(graph, vertex))
    return visited
```

## core types: list

```python
def neighbours(graph, v):
    for u in graph[v]:
        yield u

def gfs(graph, start, func=lambda g, v: None, mode='bfs'):
    visited, items = set(), [start]
    while items:
        vertex = items.pop(0 if mode == 'bfs' else -1)
        if vertex not in visited:
            visited.add(vertex)
            func(graph, vertex)
            items.extend(neighbours(graph, vertex))
    return visited
```

## core types: tuple

```python
tpl = (42, -7, 3.14, 'lorem', [0], tuple())
```
<small>

| function | description |
|--|--|
| T.count(value) -> integer | return number of occurrences of value |
| T.index(value, [start, [stop]]) -> integer | return first index of value |

</small>

::: notes
It is comparable to persistent linked list.
There is not restriction on contained types.
It is immutable.
:::

## core types: tuple

```python
>>> x = (1, 'lorem')
>>> y = x + ('ipsum', 42, 3.14)
>>> x
(1, 'lorem')
>>> y
(1, 'lorem', 'ipsum', 42, 3.14)
```

## slices

```python
>>> lst = list(range(10))
>>> lst[1:4]
[1, 2, 3]
>>> lst[:4]
[0, 1, 2, 3]
>>> lst[7:]
[7, 8, 9]
>>> lst[:-4]
[0, 1, 2, 3, 4, 5]
>>> lst[-2:]
[8, 9]
>>> lst[:]
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
```

## slices

```python
>>> lst[::2]
[0, 2, 4, 6, 8]
>>> lst[::-1]
[9, 8, 7, 6, 5, 4, 3, 2, 1, 0]
```

## TASK

Write a function that rotates a list by a given offset using slicing

```python
def rotate(lst, offset):
  """
  implement me
  """

lst = list(range(5))
print(rotate(lst, 2))
```
```
[2, 3, 4, 0, 1]
```

::: notes
[solution](https://gitlab.com/attu/python_intro/blob/master/examples/basics/rotate.py)
:::

## core types: dictionary

```python
dct = {1:'lorem', 'ipsum':3.14, 42:dict(x='y')}
dct['key'] = 'value'
```

## core types: dictionary

<small>

| function | description |
|--|--|
| D.get(k[,d]) | D[k] if k in D, else d. d defaults to None |
| D.items() | a set-like object providing a view on D's items |
| D.keys() | a set-like object providing a view on D's keys |
| D.values() | an object providing a view on D's values |
| D.update([E, ]**F) | update D from dict/iterable E and F |

</small>

## core types: set

```python
st = {'lorem', 'ipsum',  'dolor'}
```

## core types: set

<small>

| function | description |
|--|--|
| add(...) | add an element to a set |
| remove(...) | remove an element from a set; it must be a member |
| discard(...) | remove an element from a set if it is a member |

</small>

::: notes
Bloom filter: https://en.wikipedia.org/wiki/Bloom_filter
:::

## core types: set

<small>

| function | description |
|--|--|
| difference(...) | return the difference of two or more sets as a new set |
| intersection(...) | return the intersection of two sets as a new set |
| union(...) | return the union of sets as a new set |
| isdisjoint(...) | return True if two sets have a null intersection |
| issubset(...) | report whether another set contains this set |
| issuperset(...) | report whether this set contains another set |

</small>

## core types: None

```python
x = None
```

## core types: bool

```python
True, False

assert not None
assert not 0
assert not False
assert not list()
assert not tuple()
assert not dict()
assert not set()
```

## core types: TextIOWrapper

<small>

| function | description |
|--|--|
| open(file, mode='r') |  Open file and return a stream. |
| close(self, /) | Flush and close the IO object. |
| fileno(self, /) | Returns underlying file descriptor if one exists. |
| read(self, size=-1, /) | Read at most n characters from stream. |
| readline(self, size=-1, /) | Read until newline or EOF. |
| write(self, text, /) | Write string to stream. |
| seek(self, cookie, whence=0, /) | Change stream position. |
| tell(self, /) | Return current stream position. |

</small>

## core types: TextIOWrapper

```python
f = open('lorem', mode='r')
```

```text
FileNotFoundError: [Errno 2] No such file or directory: 'lorem'
```

## core types: TextIOWrapper

```python
f = open('lorem', mode='w')
f.write('ipsum')
f.close()

g = open('lorem', mode='r')
print(g.fileno(), g.readline())
g.close()
```

```text
3 ipsum
```
::: notes
same pattern for all resources with two related function calls
which are happen in separate time moments
:::

## other core types

- object
- module
- type
- function
- slice

## lambda expression

`lambda` *argument1, argument2,... argumentN* : *expression using arguments*

```python
pred = lambda x: x % 2 == 0
```

## why and when to use lambda?

- it is an expression, not a statement

```python
>>> sorted(lst, key=lambda x: abs(x-7))
[7, 6, 8, 5, 9, 4, 3, 2, 1, 0]
```

::: notes
can always make a named function, use lambdas when short syntax does not affects readability
:::

## class definition

```python
class MyClass:
  pass

obj = MyClass()

import inspect
print(inspect.getmro(MyClass))
```

## class definition

<small>

| version | output |
|--|--|
| python2 | `(<class __main__.MyClass at 0x7f822ac23940>,)` |
| python3 | `(<class '__main__.MyClass'>, <class 'object'>)` |

</small>

## classes: construction

```python
class MyClass:
  def __init__(self, x):
    print('MyClass.__init__', x)
    self.value = x

obj = MyClass(42)
```

::: notes
By convention first method argument is called `self`.
It is equivalent to c++ this pointer but always explicit.
There are 3 functions that are creating an object:
:::

## classes: construction

- `metaclass.__call__`
- `__new__`
- `__init__`

## classes: inheritance

```python
class Base:
  def foo(self, x): print('Base', id(self), x)
  def bar(self, y): print('Base', id(self), y)

class Derived(Base):
  def bar(self, y): print('Derived', id(self), y)

obj = Derived()
obj.foo(42)
obj.bar(7)
```
```
140479922802872 Base 42
140479922802872 Derived 7
```

::: notes
- duck typing
- no argument based function overloading
:::

## classes: delegating

```python
class Base:
  def __init__(self, x):
    print(id(self), 'Base', x)

class Derived(Base):
  def __init__(self, *args):
    print(id(self), 'Derived', *args)
    super(Derived, self).__init__(*args)

obj = Derived(42)
```
```
140257075269528 Derived 42
140257075269528 Base 42
```

## classes: public methods

```python
class MyClass:
  def set_value(self, x): self.__value = x
  def get_value(self): return self.__value
```

## TASK

Write a class hierarchy of `Rectangle` and `Square` with dimension setters,
perimeter and area getters

## classes: properties

```python
class Connection:
  def __init__(self, host, port):
    self.host = str(host)
    self.port = int(port)

  @property
  def address(self):
    return self.host + ':' + str(self.port)

  @address.setter
  def address(self, new):
    a,b = new.split(':')
    self.host = str(a)
    self.port = int(b)
```

## classes: properties

```python
con = Connection('localhost', 1234)
print(con.host, con.port, con.address)
con.address = '127.0.0.1:4321'
print(con.host, con.port, con.address)
```
```text
localhost 1234 localhost:1234
127.0.0.1 4321 127.0.0.1:4321
```

## staticmethod and classmethod

```python
class Connection:
  @staticmethod
  def localhost(): return '127.0.0.1'

  @classmethod
  def add(cls, addr):
    if hasattr(cls, 'connections'):
      cls.connections.add(addr)
    else:
      cls.connections = {addr}

  @classmethod
  def listall(cls):
    return cls.connections or set()
```

## staticmethod and classmethod

```python
Connection.add(Connection.localhost())
print(Connection.listall())
```
```text
{'127.0.0.1'}
```

::: notes
staticmethod works without an isinstance
classmethod gets class isinstance instead of self
:::

## classes: modifying on runtime

```python
class MyClass:
  pass

obj = MyClass()

assert not hasattr(MyClass, 'foo')
assert not hasattr(obj, 'foo')
```

## classes: modifying on runtime

```python
MyClass.foo = lambda self: print('foo called')
assert hasattr(obj, 'foo')
obj.foo()
```
```text
foo called
```

## operators: compare

<small>

| operator | function signature |
|--|--|
| lhs < rhs| `object.__lt__(lhs, rhs)` |
| lhs <= rhs | `object.__le__(lhs, rhs)` |
| lhs == rhs | `object.__eq__(lhs, rhs)` |
| lhs !=  rhs | `object.__ne__(lhs, rhs)` |
| lhs > rhs | `object.__gt__(lhs, rhs)` |
| lhs >= rhs | `object.__ge__(lhs, rhs)` |

</small>

## operators: access

<small>

| operator | function signature |
|--|--|
| obj.member | `object.__getattr__(obj, name)` |
| obj.member = value | `object.__setattr__(obj, name, value)` |
| del obj.member | `object.__delattr__(obj, name)` |
| obj[key] | `object.__getitem__(obj, key)` |
| obj[key] = value | `object.__setitem__(obj, key, value)` |
| del obj[key] | `object.__delitem__(obj, key)` |

</small>

::: notes
good to make proxy objects
:::

## operators: arithmetic

<small>

| operator | function signature |
|--|--|
| lhs + rhs | `object.__add__(lhs, rhs)` |
| lhs - rhs | `object.__sub__(lhs, rhs)` |
| lhs * rhs | `object.__mul__(lhs, rhs)` |
| lhs / rhs | `object.__truediv__(lhs, rhs)` |
| lhs // rhs | `object.__floordiv__(lhs, rhs)` |
| lhs & rhs | `object.__and__(lhs, rhs)` |
| lhs ^ rhs | `object.__xor__(lhs, rhs)` |
| lhs &#124; rhs | `object.__or__(lhs, rhs)` |

</small>

## operators: arithmetic

<small>

| operator | function signature |
|--|--|
| lhs += rhs | `object.__iadd__(lhs, rhs)` |
| lhs -= rhs | `object.__isub__(lhs, rhs)` |
| lhs *= rhs | `object.__imul__(lhs, rhs)` |
| lhs /= rhs | `object.__itruediv__(lhs, rhs)` |
| lhs //= rhs | `object.__ifloordiv__(lhs, rhs)` |
| lhs &= rhs | `object.__iand__(lhs, rhs)` |
| lhs ^= rhs | `object.__ixor__(lhs, rhs)` |
| lhs &#124;= rhs | `object.__ior__(lhs, rhs)` |

</small>

::: notes
"i" for inplace
:::

## operators: conversion

<small>

| operator | function signature |
|--|--|
| repr(obj) | `object.__repr__(lhs, rhs)` |
| str(obj) | `object.__str__(lhs, rhs)` |
| hash(obj) | `object.__hash__(lhs, rhs)` |
| if obj: foo() | `object.__bool__(lhs, rhs)` |

</small>

::: notes
operator bool does same thing as safe bool idiom in old c++
repr is to show object representation i.e. with debug information
str does to string conversion
:::

## TASK

Write a class that supports matrix arithmetic operations:

- addition
- multiplication

::: notes
[rosetta code](http://rosettacode.org/wiki/Matrix_multiplication)
[solution](https://gitlab.com/attu/python_intro/blob/master/examples/basics/matrix.py)
:::

## exceptions

when to use exceptions?

- error handling
- control flow (?)

::: notes
In general - use exceptions when it allows to reduce boilerplate
and focus on function responsibility
:::

## exceptions

```python
import sys

x = [1,2,3,4]
try:
  i = x[4]
except IndexError as e:
  print(e, file=sys.stderr)
  i = 7

print(i)
```

::: notes
use except not only to print exception but also to do error handling!
:::

## exceptions

```python
try:
  import very_usefull_module_v2
except ImportError as e:
  print('using old')
  import very_usefull_module_v1
```

## exceptions

```python
try:
  foo()
except (ExceptionA, ExceptionB) as ab:
  handle_A_and_B(ab)
except ExceptionC as c:
  handle_C(c)
else:
  handle_no_errors()
finally:
  handle_whatever()
```

## exceptions

**`Exception`**,

`ArithmeticError`, `AssertionError`, `AttributeError`, `ImportError`, `IndexError`,
`KeyError`, `OSError`, `RuntimeError`, `SyntaxError`

::: notes
Do not to pokemon handling!
:::

## exceptions

`StopIteration`

Raised by built-in function next() and an iterator’s __next__() method
to signal that there are no further items produced by the iterator.

## exceptions

```python
class MyOwnException(RuntimeError):
  pass
```

## exceptions

```python
def foo(number):
  if not isinstance(number, int):
    raise RuntimeError(f'expecting an int, got {type(number)}')
  assert number > 0
```

## decorators

- function decorators: function that takes a function and returns a function
- class decorators: function that takes a class and return a class

Both function and class decorator should rebind original reference

## decorators

```python
def call_twice(f):
  def wrapper(*args, **kwds):
    return f(f(*args, **kwds))
  return wrapper
```

## decorators

```python
def foo(x):
  print('foo', x)
  return x

foo = call_twice(foo)
foo(42)
```
```text
foo 42
foo 42
```

## decorators

```python
@call_twice
def foo(x):
  print('foo', x)
  return x

foo(42)
print(foo.__name__)
```
```text
foo 42
foo 42
wrapper
```

## decorators

```python
from functools import wraps

def call_twice(f):
  @wraps(f)
  def wrapper(*args, **kwds):
    return f(f(*args, **kwds))
  return wrapper
```
```text
foo 42
foo 42
foo
```

## decorators

```python
from functools import wraps

def add_to_result(n):
  def decor(f):
    @wraps(f)
    def wrapper(*args, **kwds):
      return n + f(*args, **kwds)
    return wrapper
  return decor
```

## decorators

```python
@add_to_result(42)
def foo(x):
  return x

@add_to_result('aqq ')
def bar(x):
  return str(x)

print(foo(42))
print(bar(42))
```
```
84
aqq 42
```

## TASK

Write a decorator that accepts a message as argument.
Decorated functions should log message followed by function name, arguments and return value.

::: notes
[solution](https://gitlab.com/attu/python_intro/blob/master/examples/basics/decorator.py)
:::

## generators

When to use generators:

- infinite ranges
- lazy evaluation
- suspending call

::: notes
lazy vs eager
coroutines
:::

## generators

- `yield` instead of `return`
- provides iteration protocol:
  - `__next__`
  - `StopIteration`

## generators

```python
def lazy_range(start, stop, step):
  while True:
    if start >= stop: raise StopIteration()
    yield start
    start += step

print([x for x in lazy_range(4, 12, 2)])
```
```text
[4, 6, 8, 10]
```

## generators

```python
gen = lazy_range(4, 12, 2)
print([x for x in gen])
print([x for x in gen])
```
```text
[4, 6, 8, 10]
[]
```

## generators

```python
gen = lazy_range(4, 12, 2)
print(next(gen))
print(next(gen))
print(next(gen))
```
```text
4
6
8
```

## generators

```python
gen = lazy_range(4, 12, 2)
help(gen)
```

<small>

| function | description |
|--|--|
| `__iter__(self)` | Implement iter(self). |
| `__next__(self)` | Implement next(self). |
| `send(arg)` | sends 'arg' into generator, return next yielded value or raise StopIteration. |

</small>

## generators

```python
def receiver(x):
  i = 0
  y = 0
  while i < x:
    y = yield i
    i += y
```

## generators

```python
gen = receiver(53)
print(next(gen))
print(gen.send(11))
print(gen.send(13))
print(gen.send(17))
print(gen.send(23))
```
```text
0
11
24
41
Traceback (most recent call last):
  File "prog.py", line 13, in <module>
    print(gen.send(23))
StopIteration
```

## generators

```python
def g(x):
  yield from range(x, 0, -1)
  yield from range(x)

def h(x):
  for i in range(x, 0, -1): yield i
  for i in range(x): yield i

print([x for x in g(4)])
print([x for x in h(4)])
```

::: notes
yield from only in python3
:::

## generator expressions

```python
>>> next((x for x in range(50) if x > 30), 42)
31
```
::: notes
additional parenthesis if generator expression
is not the sole argument of next function
:::

## range

```python
>>> list(range(10))
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
```

## functional

- `map(function, iterable, ...)`
- `filter(function or None, iterable)`
- `functools.reduce(function, sequence[, initial]) -> value`

::: notes
std::reduce since c++17, std::accumulate, std::transform, erase-remove idiom
:::

## list comprehension
### identity

```python
>>> [i for i in range(10)]
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

>>> list(map(lambda i: i, range(10)))
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
```

::: notes
identity function similar to `echo` does nothing
:::

## list comprehension
### map

```python
>>> [i*2 for i in range(10)]
[0, 2, 4, 6, 8, 10, 12, 14, 16, 18]

>>> list(map(lambda i: i*2, range(10)))
[0, 2, 4, 6, 8, 10, 12, 14, 16, 18]
```

## list comprehension
### map

```python
>>> [i*2 if i<2 else -i for i in range(10)]
[0, 2, -2, -3, -4, -5, -6, -7, -8, -9]

>>> list(map(lambda i: i*2 if i<2 else -i, range(10)))
[0, 2, -2, -3, -4, -5, -6, -7, -8, -9]
```

## list comprehension
### filter

```python
>>> [i for i in range(10) if i % 2 != 0]
[1, 3, 5, 7, 9]

>>> list(filter(lambda i: i % 2 != 0, range(10)))
[1, 3, 5, 7, 9]
```

## list comprehension
### filter & map

```python
>>> [i*i for i in range(10) if i % 2 != 1]
[0, 4, 16, 36, 64]

>>> list(map(lambda i: i*i, filter(lambda i: i % 2 != 1, range(10))))
[0, 4, 16, 36, 64]
```

## list comprehension
### nesting

```python
>>> [(a, b) for a in range(3) for b in range(3,5)]
[(0, 3), (0, 4), (1, 3), (1, 4), (2, 3), (2, 4)]

>>> x = []
>>> for a in range(3):
...   for b in range(3,5):
...     x.append((a,b))
...
>>> x
[(0, 3), (0, 4), (1, 3), (1, 4), (2, 3), (2, 4)]
```

## dict comprehension

```python
x = [42, 3.14, 'lorem', True, None]

dct = {type(e).__name__:e for e in x}
print(dct)
```
```text
{'int': 42, 'float': 3.14, 'str': 'lorem', 'bool': True, 'NoneType': None}
```

## TASK

Write list comprehension that for nested list `x`:

- removes sublists of size greater than 4
- flattens list
- negate odd items
- multiply even items by itself


```python
>>> x = [[0, 1, 2], [0, 1, 2, 3, 4], [0, 1, 2, 3]]
>>> expected_output = [0, -1, 4, 0, -1, 4, -3]
```
::: notes
[solution](https://gitlab.com/attu/python_intro/blob/master/examples/basics/list_comprehension.py)
sometimes is better to write a function cause nested list comprehensions decrease readability
:::

## built-in functions: `all`, `any`

```python
>>> all((42, True, 'lorem'))
True
>>> all((42, True, 'lorem', []))
False
>>> any((42, True, 'lorem', []))
True
```

## built-in functions: `chr`, `ord`

```python
>>> [chr(i) for i in range(65,72)]
['A', 'B', 'C', 'D', 'E', 'F', 'G']
>>> [ord(c) for c in 'lorem']
[108, 111, 114, 101, 109]
```

## built-in functions: `map`, `filter`, `reduce`

```python
>>> list(map(lambda c: chr(c), range(65, 72)))
['A', 'B', 'C', 'D', 'E', 'F', 'G']
>>> list(filter(lambda c: c%2==1, range(65, 72)))
[65, 67, 69, 71]
>>> sum(range(65, 72))
476
```

## built-in functions: `map`, `filter`, `reduce`

```python
>>> from functools import reduce
>>> from itertools import accumulate
>>> reduce(lambda s, v: s + 2**v, range(8), 0)
255
>>> [x + 1 for x in accumulate(range(8), lambda s, v: s + 2**v)]
[1, 3, 7, 15, 31, 63, 127, 255]
```

::: notes
in cpp accumulate is something different
:::

## built-in functions: `type`, `isinstance`, `issubclass`

```python
>>> class MyClass:pass
>>> obj = MyClass()
>>> type(obj)
__main__.MyClass
>>> type(type(obj))
type
```

::: notes
python has very rich reflection system
:::

## built-in functions: `type`, `isinstance`, `issubclass`

```python
>>> type(obj) == MyClass
True
>>> isinstance(obj, (MyClass, int))
True
>>> issubclass(type(obj), (float, int))
False
```

## built-in functions: `hasattr`, `getattr`

```python
>>> hasattr(list, 'append')
True
>>> hasattr(list, 'add')
False
```
## built-in functions: `hasattr`, `getattr`

```python
>>> lst = []
>>> f = getattr(lst, 'add', lst.append)
>>> f(42)
>>> lst
[42]
>>> f = getattr(lst, 'append', lst.append)
>>> f(7)
>>> lst
[42, 7]
```

## built-in functions: `vars`

```python
>>> import functools
>>> from types import FunctionType
>>> {x for x,f in vars(functools).items()
...    if isinstance(f, FunctionType) and not x.startswith('_')}
{'get_cache_token',
'lru_cache',
'namedtuple',
'recursive_repr',
'singledispatch',
'total_ordering',
'update_wrapper',
'wraps'}
```

::: notes
only if obj has __dict__ member
classes with __slots__ member does not support vars
:::

## built-in functions: `hash`, `id`

```python
>>> tpl = (1,2,3)
>>> other = (1,2,3)
>>> hash(tpl) == hash(other)
True
>>> id(tpl) == id(other)
False
```

## built-in functions: `open`

```python
>>> f = open('/tmp/file', 'w')
>>> f.write('hello')
5
>>> f.close()
```
```python
>>> f = open('/tmp/file', 'r')
>>> f.read()
'hello'
>>> f.close()
```

## built-in functions: `open`

```python
>>> with open('/tmp/file', 'w') as f:
...   f.write('hello, world!')
>>> with open('/tmp/file', 'r') as f:
...   print(f.read())
hello, world!
```

::: notes
with statement works like RAII in c++
:::

## built-in functions: `sorted`

```python
>>> words = ['lorem', 'ipsum', 'dolor', 'sit', 'amet']
>>> sorted(words, key=lambda w: w.count('o'), reverse=True)
['dolor', 'lorem', 'ipsum', 'sit', 'amet']
```

## TASK

Write a function that returns top `n` occurring chars in `text`.

```text
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
```
```python
def most_common(n, txt):
  """
  implement me
  """
print(most_common(5, lorem))
```

## TASK

```text
[' ', 'i', 'e', 't', 'o']
```

::: notes
[solution](https://gitlab.com/attu/python_intro/blob/master/examples/basics/counter.py)
:::

## collections

<small>

|name|description|
|-|-|
| Counter| dict subclass for counting hashable objects|
| ChainMap| dict-like class for creating a single view of multiple mappings|
| deque| list-like container with fast appends and pops on either end|
| defaultdict| dict subclass that calls a factory function to supply missing values|
| OrderedDict| dict subclass that remembers the order entries were added|
| namedtuple| factory function for creating tuple subclasses with named fields|

</small>

## Counter

```text
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
```

```python
from collections import Counter
cnt = Counter(lorem)
print(cnt.most_common(3))
```
```text
[(' ', 62), ('i', 42), ('e', 37)]
```

::: notes
stores a dict where keys are the counted items and values are occurrences
:::

## ChainMap

```python
from collections import ChainMap

x = {c:ord(c) for c in 'lorem'}
y = {i:c for c,i in x.items()}

m = ChainMap(dict(), x, y)
print(m['l'], m[111])
```
```text
108 o
```

::: notes
setitem changes underlying dict
new map item is added into first dict
:::

## namedtuple

```python
from collections import namedtuple

Rectangle = namedtuple('Rectangle', ['a', 'b'])
rec = Rectangle(7, b=42)

print(rec[0], rec[1])
print(rec.a * rec.b)
print(repr(rec))
```
```text
7 42
294
Rectangle(a=7, b=42)
```

## metaclasses

```python
class Meta(type):
  def __call__(meta, *args, **kwds):
    return type.__call__(meta, *args, **kwds)
  def __init__(cls, name, bases, dct):
    super(Meta, cls).__init__(name, bases, dct)
  def __new__(meta, name, bases, dct):
    return super(Meta, meta).__new__(meta, name, bases, dct)


class Foo(metaclass=Meta):
  pass
```

## metaclasses

```python
class Meta(type):

  def __new__(meta, name, bases, dct, **kwds):
    def setter(self, name, value):
      if name in kwds.keys():
        assert isinstance(value, kwds[name])
      return object.__setattr__(self, name, value)
    dct['__slots__'] = list(kwds.keys())
    dct['__setattr__'] = setter

    return super(Meta, meta).__new__(meta, name, bases, dct)
```

## metaclasses

```python
class Foo(metaclass=Meta, x=int):
  pass

f = Foo()
f.x = 42
```

## metaclasses

[code sample](https://gitlab.com/attu/python_intro/blob/master/examples/basics/meta_typed.py)

```python
f.x = 'lorem'
```
```text
Traceback (most recent call last):
  File "script.py", line 18, in <module>
    f.x = 'lorem'
  File "script.py", line 6, in setter
    assert isinstance(value, kwds[name])
AssertionError
```

## metaclasses

```python
f.y = 42
```
```text
Traceback (most recent call last):
  File "script.py", line 18, in <module>
    f.y = 42
  File "script.py", line 7, in setter
    return object.__setattr__(self, name, value)
AttributeError: 'Foo' object has no attribute 'y'
```

# Packaging

## module

File under `PYTHONPATH` or in a _package_ in `PYTHONPATH`:

- py
- pyc, pyo
- so, dll
- zip
- ...

## modules

<small>

- `import module_name`
- `import module_name as alias`
- `from module_name import item_name`
- `from module_name import item_name as alias`
- `from module_name import *`

</small>

## modules

```python
$ cat mymodule.py
def foo():
  print('mymodule.foo')

def bar():
  print('mymodule.bar')

print('hello, world!')
```

## modules

```python
$ cat script.py
import mymodule
from mymodule import bar

mymodule.foo()
bar()
```
```text
$ python ./script.py
hello, world!
mymodule.foo
mymodule.bar
```

## modules

```python
if __name__ == '__main__':
  print('hello from main')
```

## modules

```python
X = 1
Y = 2
Z = 3

__all__ = ['X', 'Y']
```

## modules

```python
from mymodule import *

print(X, Y)
print(Z)
```
```text
1 2
Traceback (most recent call last):
  File "prog.py", line 4, in <module>
    print(Z)
NameError: name 'Z' is not defined
```

## modules

```python
>>> import sys
>>> 'docker' in sys.modules
False
>>> import docker
>>> 'docker' in sys.modules
True
>>> sys.modules['docker'] == docker
True
>>> sys.modules['docker']
<module 'docker' from '/usr/lib/python2.7/site-packages/docker/__init__.pyc'>
```

## packages

```text
.
└── sound                  Top-level package
    ├── effects            Subpackage for sound effects
    │   ├── echo.py
    │   ├── __init__.py
    │   └── surround.py
    ├── formats            Subpackage for file formats
    │   ├── __init__.py
    │   ├── wavread.py
    │   └── wavwrite.py
    └── __init__.py        Initialize the sound package
```

## packages

```python
import sound.effects.echo
import sound.effects.surround
from sound.effects import *
```

## pip

A tool for installing and managing Python packages

::: notes
like rust cargo or conan in c++
:::

## pip

<small>

|command|description|
|--|--|
| help| Show help for commands.|
| install| Install packages.|
| uninstall| Uninstall packages.|
| freeze| Output installed packages in requirements format.|
| list| List installed packages.|
| show| Show information about installed packages.|
| search| Search PyPI for packages.|

</small>

## pip

<small>

|option|description|
|--|--|
| -r,--requirement `file`| Install from the given requirements file.|
| -d,--download `dir`| Download packages into `dir` instead of installing them.|
| -U, --upgrade| Upgrade all packages to the newest available version.|
| --no-deps| Don't install package dependencies.|
| -i,--index-url `url`| Base URL of Python Package Index (default https://pypi.python.org/simple/).|
| --extra-index-url `url`| Extra URLs of package indexes to use in addition to --index-url.|

</small>

## pip

`pip.conf`

```text
[global]
timeout = 60
index-url = http://download.zope.org/ppix
```

## pip

```text
$ pip install bson
Collecting bson
  Downloading bson-0.5.0.tar.gz
Collecting pytz>=2010b (from bson)
  Downloading pytz-2017.3-py2.py3-none-any.whl (511kB)
    100% |████████████████████████████████| 512kB 680kB/s
Requirement already satisfied: six>=1.9.0 in ./.virtualenvs/py3/lib/python3.6/site-packages (from bson)
Building wheels for collected packages: bson
  Running setup.py bdist_wheel for bson ... done
  Stored in directory: /home/kwozniak/.cache/pip/wheels/94/c5/28/4d678b696f8fb2cde3bb96c972ffb72c149d5a96be956d9725
Successfully built bson
Installing collected packages: pytz, bson
Successfully installed bson-0.5.0 pytz-2017.3
```

## packages

`setup.py`

```python
from setuptools import setup

setup(
    name='mypackage',
    url='https://github.com/edison/mypackage',
    description='library provides tcp communication',
    author='Dilbert',
    author_email='dilbert@edison.com',
    version='3.1.2',
    install_requires=['pyzmq>=16.0'],
    packages=['mypackage'],
)
```

::: notes
semver
:::

## virtualenv

```text
$ python3 -m venv myenv
$ source myenv/bin/activate
(myenv) $ pip list installed
pip (9.0.1)
pkg-resources (0.0.0)
setuptools (32.3.1
(myenv) $ deactivate
```

# Unittest

## test case

```python
import unittest

class MyTest(unittest.TestCase):

  def test_nothing(self):
    pass

if __name__ == '__main__':
  unittest.main()
```
```text
 .
 --------------------------------------------------------------
 Ran 1 test in 0.000s

 OK
```

## test case

`tree.py`

```python
from collections import defaultdict

def tree(): return defaultdict(tree)
```

## test case

```python
import unittest
from tree import tree
class TreeTest(unittest.TestCase):
  def setUp(self):
    self.tree = tree()
  def test_sizeOfEmptyTreeIsZero(self):
    self.assertEqual(len(self.tree), 0)
  def test_EmptyTreeConvertsToFalse(self):
    self.assertFalse(self.tree)
  def test_canCreateNestedNodes(self):
    self.tree['outer']['nested'] = 'lorem'
    self.assertTrue('outer' in self.tree)
    self.assertTrue('nested' in self.tree['outer'])
    self.assertEqual(self.tree['outer']['nested'], 'lorem')
```

## test case

[permlink](https://wandbox.org/permlink/osyUOllJEMkeu151)

## unittest: assertions

<small>

|Method|Checks that|
|--|--|
| assertEqual(a, b)| a == b|
| assertNotEqual(a, b)| a != b|
| assertTrue(x)| bool(x) is True|
| assertFalse(x)| bool(x) is False|
| assertIsNone(x)| x is None|
| assertIn(a, b)| a in b|
| assertRaises(exc, fun, *args, **kwds)| fun(*args, **kwds) raises exc|

</small>

## TASK

Write and make unit tests for a function that visits all nodes of a tree.

```python
def walk(function, tree):
  """
  implement me
  """
```

::: notes
[test](https://gitlab.com/attu/python_intro/blob/master/examples/tests/test_tree.py)
[impl](https://gitlab.com/attu/python_intro/blob/master/examples/tests/tree.py)
:::

## unittest: decorators

<small>

|decorator|description|
|--|--|
| @unittest.skip(reason)| Unconditionally skip the decorated test.|
| @unittest.skipIf(condition, reason)| Skip the decorated test if condition is true.|
| @unittest.skipUnless(condition, reason)| Skip the decorated test unless condition is true.|
| @unittest.expectedFailure| If the test fails when run, the test is not counted as a failure.|

</small>

## unittest: mock

```python
>>> from unittest.mock import MagicMock
>>> thing = ProductionClass()
>>> thing.method = MagicMock(return_value=3)
>>> thing.method(3, 4, 5, key='value')
3
>>> thing.method.assert_called_with(3, 4, 5, key='value')
```

## unittest: mock

```python
>>> mock = Mock(side_effect=KeyError('foo'))
>>> mock()
Traceback (most recent call last):
 ...
KeyError: 'foo'
```

::: notes
iterable side_effect returns one element after each call
:::

## unittest: patch

```python
>>> from unittest.mock import patch
>>> @patch('module.ClassName2')
... @patch('module.ClassName1')
... def test(MockClass1, MockClass2):
...     module.ClassName1()
...     module.ClassName2()
...     assert MockClass1 is module.ClassName1
...     assert MockClass2 is module.ClassName2
...     assert MockClass1.called
...     assert MockClass2.called
...
>>> test()
```

# Functional

## itertools

<small>

|Iterator|Results|Example|
|--|--|--|
| count(start [, step])| start, start+step, start+2*step, ...| count(10) --> 10 11 12 13 14 ...|
| cycle(p)| p p0, p1, ..., plast, p0, p1, ...| cycle('ABCD') --> A B C D A B C D ...|
| repeat(elem [, n])| elem, elem, elem, ...| endlessly or up to n times repeat(10, 3) --> 10 10 10|

</small>

## itertools

<small>

|Iterator|Results|Example
|--|--|--|
| accumulate(p [, func])| p0, p0+p1, p0+p1+p2, ...| accumulate([1,2,3,4,5]) --> 1 3 6 10 15|
| chain(p, q, ...)| p0, p1, ..., plast, q0, q1, ...| chain('ABC', 'DEF') --> A B C D E F|
| compress(data, selectors)| (d[0] if s[0]), (d[1] if s[1]), ...| compress('ABCDEF', [1,0,1,0,1,1]) --> A C E F|
| dropwhile(pred, seq)| seq[n], seq[n+1], starting when pred fails| dropwhile(lambda x: x<5, [1,4,6,4,1]) --> 6 4 1|
| takewhile(pred, seq)| seq[0], seq[1], until pred fails| takewhile(lambda x: x<5, [1,4,6,4,1]) --> 1 4|
| tee(it, n)| it1, it2, ..., itn| splits one iterator into n|

</small>

## itertools

<small>

|Iterator|Results
|--|--|
|product(p, q, ..., [repeat=1])|cartesian product, equivalent to a nested for-loop|
|permutations(p[, r])|r-length tuples, all possible orderings, no repeated elements|
|combinations(p, r)|r-length tuples, in sorted order, no repeated element|

</small>

## TASK

Given an array of integers, find length of the largest subarray with sum equals to 0

::: notes
<https://www.geeksforgeeks.org/find-the-largest-subarray-with-0-sum/>
<https://wandbox.org/permlink/0Tibqto3gKeEOfSi>
[solution](https://gitlab.com/attu/python_intro/blob/master/examples/functional/zero_subset.py)
:::

## functools

`@functools.lru_cache(maxsize=128, typed=False)`

```python
@lru_cache(maxsize=32)
def get_pep(num):
    'Retrieve text of a Python Enhancement Proposal'
    resource = 'http://www.python.org/dev/peps/pep-%04d/' % num
    try:
        with urllib.request.urlopen(resource) as s:
            return s.read()
    except urllib.error.HTTPError:
        return 'Not Found'
```

## functools

```python
>>> for n in 8, 290, 308, 320, 8, 218, 320, 279, 289, 320, 9991:
...     pep = get_pep(n)
...     print(n, len(pep))

>>> get_pep.cache_info()
CacheInfo(hits=3, misses=8, maxsize=32, currsize=8)
```

## functools

### `@singledispatch`

[singledispatch](https://docs.python.org/3/library/functools.html#functools.singledispatch)
transforms a function into a single-dispatch generic function.

::: notes
can register overloads dispatched on a type of first argument
:::

## functools

### `@wraps(wrapped, ...)`

Preserves function reflection info after decorating with wrapper

## context manager

The execution of the with statement with one “item” proceeds as follows:

<small>

1. The context expression is evaluated to obtain a context manager.
2. The context manager’s `__exit__()` is loaded for later use.
3. The context manager’s `__enter__()` method is invoked.
4. If a target was included in the with statement, the return value from `__enter__()` is assigned to it.
5. The suite is executed.
6. The context manager’s `__exit__()` method is invoked.

</small>

## context manager

```python
with open('filename', 'r') as f:
  text = f.read()
```

## contextlib

```python
from contextlib import contextmanager

@contextlib.contextmanager
def cm(x):
  print('called at enter')
  yield x
  print('called at exit')

with cm(7) as x:
  print('suite got', x)
```
```text
called at enter
suite got 7
called at exit
```

# Strings

## string

<small>

|method|description|
|--|--|
|S.find(sub[, start[, end]]) -> int|Return the lowest index in S where substring sub is found.|
|S.count(sub[, start[, end]]) -> int|Return the number of non-overlapping occurrences of substring sub in string.|
|S.startswith(prefix[, start[, end]]) -> bool|Return True if S starts with the specified prefix, False otherwise.|
|S.endswith(suffix[, start[, end]]) -> bool|Return True if S ends with the specified suffix, False otherwise.|
|S.lstrip([chars]) -> str|Return a copy of the string S with leading whitespace removed.|
|S.rstrip([chars]) -> str|Return a copy of the string S with trailing whitespace removed.|

</small>

## string

<small>

|method|description|
|--|--|
|S.partition(sep) -> (head, sep, tail)|Search for the separator sep in S, and return the part before it.|
|S.format(*args, **kwargs) -> str|Return a formatted version of S.|
|S.join(iterable) -> str|Return a string which is the concatenation of the strings in the iterable.|
|S.replace(old, new[, count]) -> str|Return a copy of S with all occurrences of substring old replaced by new.|
|S.split(sep=None, maxsplit=-1) -> list of strings|Return a list of the words in S, using sep as the delimiter string.|

</small>

## string

```python
text = """
Lorem ipsum dolor sit amet,
consectetur adipiscing elit,
sed do eiusmod tempor incididunt
ut labore et dolore magna aliqua.
"""

def words(txt):
  return text.split()

print(words(text)[:5])
```
```text
['Lorem', 'ipsum', 'dolor', 'sit', 'amet,']
```

## string

```python
print(text.replace('dolore ', '').replace('dolor ', ''))
```
```text
Lorem ipsum sit amet,
consectetur adipiscing elit,
sed do eiusmod tempor incididunt
ut labore et magna aliqua.
```

## string

```python
print(' '.join((w for w in text.split(sep=' ')
                if not w.startswith('dolor'))))
```
```text
Lorem ipsum sit amet,
consectetur adipiscing elit,
sed do eiusmod tempor incididunt
ut labore et magna aliqua.
```

## TASK

Write a function that converts integer to string in roman notation

```python
def to_roman(num):
  """
  implement me
  """

assert to_roman(48) == 'XLVIII'
```

::: notes
[solution](https://gitlab.com/attu/python_intro/blob/master/examples/strings/to_roman.py)
:::

## string: format

```python
print('hello {}!'.format('Dilbert'))
```
```text
hello Dilbert!
```

## string: format

```python
name = 'Dilbert'
print(f'hello {name}!')
```
```text
hello Dilbert!
```

## string: format

```python
message = 'hello {name}!, are you from {city}? bye {name}'

print(message.format(name='Dilbert', city='NYC'))
print(message.format(name='Kate', city='London'))
```
```text
hello Dilbert!, are you from NYC? bye Dilbert
hello Kate!, are you from London? bye Kate
```

## logging

```python
import logging
logging.basicConfig(format='%(asctime)-15s $ %(message)s')
```

## logging

```python
import logging
log = logging.getLogger(__name__)

city = 'Houston'
log.warning('{city}, we have a problem!'.format(city=city))
```
```text
2017-12-07 00:26:22,710 $ Houston, we have a problem!
```

## logging

<small>

|Attribute|Format|Description|
|--|--|--|
|created|%(created)f|Time when the LogRecord was created.|
|filename|%(filename)s|Filename portion of pathname.|
|funcName|%(funcName)s|Name of function containing the logging call.|
|levelname|%(levelname)s|Text logging level for the message.|
|lineno|%(lineno)d|Source line number where the logging call was issued.|
|module|%(module)s|Module name.|
|message|%(message)s|The logged message.|
|name|%(name)s|Name of the logger used to log the call.|
|process|%(process)d|Process ID.|
|thread|%(thread)d|Thread ID.|

</small>

## logging

- rich configuration
- easy formatting
- easy redirecting
- ...

## regular expressions

```python
import re

log = '2017-12-07 00:26:22,710 $ Houston, we have a problem!'

pattern = '^(?P<date>[0-9]{4}-[0-9]{2}-[0-9]{2})\s(?P<time>[0-9]{2}:[0-9]{2}:[0-9]{2},[0-9]{3})\s\$\s(?P<message>.*)$'
prog = re.compile(pattern)
f = prog.match(log)
if f:
  print('date:', f.group('date'))
  print('time:', f.group('time'))
  print('msg:', f.group('message'))
```
```text
date: 2017-12-07
time: 00:26:22,710
msg: Houston, we have a problem!
```

## TASK

Make a regexp matcher for a email footnote.
Get: name, surname, address, phone, email

```text
Ernst Stavro Blofeld
Leader, Number 1
SPECTRE
12 Rue des Mechants
75000 Paris, France
+33 1 12 34 56 78
number1@spectre.org
twitter: www.twitter.org/ernieblo
```

::: notes
solution: [regexp matcher](https://gitlab.com/attu/python_intro/blob/master/examples/strings/regex.py)
:::

## pyparsing

```python
from pyparsing import *

name = Word(alphanums + '_')
include = Suppress(Keyword('include')) + QuotedString('"')
namespace = typename = Combine(
  ZeroOrMore(name + Literal('::')) +
  Word(alphanums + '_[]:')
)
typedef = Group(
  Suppress(Keyword('typedef')) +
  name.setResultsName('name') +
  Suppress(Literal(':')) +
  typename.setResultsName('typename') +
).setResultsName('typedef')
```

## pyparsing

```python
grammar = \
    ZeroOrMore(include).setResultsName('includes') + \
    Optional(namespace).setResultsName('namespace') + \
    ZeroOrMore(Group(
        comment |
        description |
        typedef |
        structure |
        enumeration |
        const
    )).setResultsName('elements')

result = grammar.parseString(source)
```

## TASK

Write a chemical formula parser

```python
print(formula.parseString("H2O"))
print(formula.parseString("NaCl"))
print(formula.parseString("C2H5OH"))
```
```text
[['H', '2'], ['O', '1']]
[['Na', '1'], ['Cl', '1']]
[['C', '2'], ['H', '5'], ['O', '1'], ['H', '1']]
```

::: notes
```python
from pyparsing import Word, Optional, Group, OneOrMore, alphas

element = Word(alphas.upper(), alphas.lower(), max=2)
elementRef = Group(element + Optional(Word("0123456789"), default="1"))
formula = OneOrMore(elementRef)

print(formula.parseString("H2O"))
print(formula.parseString("NaCl"))
print(formula.parseString("C2H5OH"))
```
:::

# Serialization

## struct

<small>

|Function|Description|
|--|--|
|struct.pack(fmt, v1, v2, ...) -> bytes|Return a bytes object containing the values v1, v2, ... packed according to the format string fmt.|
|struct.unpack(fmt, buffer) -> tuple|Unpack from the buffer according to the format string fmt.|

</small>

## struct: pack

```python
import struct

def syscom_header_encode(msgId, receiver,
                         sender, size, flags, **k):
  fmt = '!IIIHH'
  return struct.pack(fmt, msgId, receiver, sender,
                     size + struct.calcsize(fmt), flags)

def syscom_encode(payload, **kw):
  payload = payload.encode('utf-8')
  header = syscom_header_encode(size=len(payload),
                                **kw['header'])
  return header + payload
```

## struct: pack

```python
from binascii import hexlify
msg = dict(payload='hello, world!',
           header=dict(msgId=0x4242,
                       receiver=0x14311200,
                       sender=0x10311000,
                       flags=0x4))
print(hexlify(syscom_encode(**msg)))
```
```text
b'000042421431120010311000001d000468656c6c6f2c20776f726c6421'
```

## struct: byte order

<small>

|Character|Byte order|Size|Alignment|
|--|--|--|--|
|@|native|native|native|
|=|native|standard|none|
|<|little-endian|standard|none|
|>|big-endian|standard|none|
|!|network (= big-endian)|standard|none|

</small>

## struct: format

<small>

|Format|C Type|Python type|Standard size|
|--|--|--|--|
|c|char|bytes of length 1|1|
|b/B|signed/unsigned char|integer|1|
|?|_Bool|bool|1|
|h/H|short/unsigned short|integer|2|
|i/I|int/unsigned int|integer|4|
|l/L|long/unsigned long|integer|4|
|q/Q|long long/unsigned long long|integer|8|
|f|float|float|4|
|d|double|float|8|
|s/p|char[]|bytes||

</small>

## struct: Unpack

```python
import struct
data = b'\x00\x00BB\x141\x12\x00\x101\x10\x00\x00\x1d\x00\x04hello, world!'

def syscom_header_decode(data):
    fmt = '!IIIHH'
    header_size = struct.calcsize(fmt)
    msgId, receiver, sender, size, flags = struct.unpack(
        fmt, data[:header_size])
    return dict(msgId=msgId, receiver=receiver, sender=sender,
                flags=flags), data[header_size:]

def syscom_payload_decode(data):
    return data.decode('utf-8')

def syscom_decode(data):
    header, data = syscom_header_decode(data)
    payload = syscom_payload_decode(data)
    return dict(header=header, payload=payload)
```

## struct: Unpack

``` python
msg = syscom_decode(data)
print(
    "header = {{'msgId': 0x{msgId:08x}, 'receiver': 0x{receiver:08x}, 'sender': {sender:08x}}}".format(
        **msg['header']))
print("payload = '{msg[payload]}'".format(msg=msg))
```
```text
header = {'msgId': 0x00004242, 'receiver': 0x14311200, 'sender': 10311000}
payload = 'hello, world!'
```

## pickle

<small>

|Function|Description|
|--|--|
|pickle.dumps(obj, ...)|Return the pickled representation of the object as a bytes object.|
|pickle.dump(obj, file, ...)|Write a pickled representation of obj to the open file object file.|
|pickle.loads(data, ...)|Return the reconstituted object hierarchy specified therein.|
|pickle.load(file, ...)|Return the reconstituted object hierarchy specified in file.|

</small>

::: notes
python specific
:::

## shelve

```python
>>> import shelve
>>> with shelve.open('shelf.db') as db:
...    db['answer'] = 42
...
```

```python
>>> import shelve
>>> with shelve.open('shelf.db') as db:
...     print(db['answer'])
...
42
```

## json

<small>

|Function|Description|
|--|--|
|json.dumps(obj, ...)|Serialize obj to a JSON formatted str.|
|json.dump(obj, file, ...)|Serialize obj as a JSON formatted stream to file.|
|json.loads(s, ...)|Deserialize s to a Python object.|
|json.load(file, ...)|Deserialize file object containing a JSON document to a Python object.|

</small>

## json: conversion table

<small>

|JSON|Python|
|--|--|
|object|dict|
|array|list|
|string|str|
|number (int)|int|
|number (real)|float|
|true|True|
|false|False|
|null|None|

</small>

## cbor2

```python
from cbor2 import dumps, loads

data = dumps(['hello', 'world'])
obj = loads(data)

with open('input.cbor', 'rb') as fp:
    obj = load(fp)

with open('output.cbor', 'wb') as fp:
    dump(obj, fp)
```

## other

- bson: encode, decode
- messagepack: packb, unpackb

# Operating system

## os

abort, chdir, chmod, chown, chroot, close, curdir, devnull, environ, errno,
fork, getcwd, getenv, geteuid, getgid, getpid, getuid, kill, listdir, mkdir,
path, remove, rmdir, setgid, setuid, uname, wait, ...

::: notes
low level, but names familiar to unix users
:::

## argparse

```python
import argparse

parser = argparse.ArgumentParser(description="X to power Y")
group = parser.add_mutually_exclusive_group()
group.add_argument("-v", "--verbose", action="store_true")
group.add_argument("-q", "--quiet", action="store_true")
parser.add_argument("x", type=int, help="the base")
parser.add_argument("y", type=int, help="the exponent")
args = parser.parse_args()
```

## argparse

```python
answer = args.x**args.y

if args.quiet:
    print(answer)
elif args.verbose:
    print(f"{args.x} to the power {args.y} equals {answer}")
else:
    print(f"{args.x}^{args.y} == {answer}")
```

## argparse

```sh
$ python3 prog.py --help
usage: prog.py [-h] [-v | -q] x y

calculate X to the power of Y

positional arguments:
  x              the base
  y              the exponent

optional arguments:
  -h, --help     show this help message and exit
  -v, --verbose
  -q, --quiet
```

## argparse

<small>

`ArgumentParser.add_argument(name or flags...[, action][, nargs][, const]
  [, default][, type][, choices][, required][, help][, metavar][, dest])`

<small>

- name or flags - Either a name or a list of option strings, e.g. foo or -f, --foo.
- action - The basic type of action to be taken when this argument is encountered at the command line.
- nargs - The number of command-line arguments that should be consumed.
- const - A constant value required by some action and nargs selections.
- default - The value produced if the argument is absent from the command line.
- type - The type to which the command-line argument should be converted.
- choices - A container of the allowable values for the argument.
- required - Whether or not the command-line option may be omitted (optionals only).
- help - A brief description of what the argument does.
- metavar - A name for the argument in usage messages.
- dest - The name of the attribute to be added to the object returned by parse_args().

</small>

</small>

## subprocess

```python
>>> import subprocess
>>> subprocess.Popen('ls /usr'.split())
bin  games  include  lib  libexec  local  sbin	share  src
```

## subprocess

<small>

`subprocess.Popen(args, executable=None, stdin=None, stdout=None,
stderr=None, shell=False, cwd=None, env=None, ...)`

</small>

## psutil

[psutil docs](https://psutil.readthedocs.io/en/latest/)

## psutil

```python
def binary(ctx):
    env = os.environ.copy()
    host, port = ctx['server'].getsockname()
    env['HOST'] = host
    env['PORT'] = str(port)
    env['PATH'] = ctx['bindir'] + ':' + env.get('PATH', '')
    env['LD_LIBRARY_PATH'] = ctx['libdir'] + \
        ':' + env.get('LD_LIBRARY_PATH', '')

    try:
        p = psutil.Popen(ctx['cmd'].split(), env=env)
        yield
    finally:
        if p: terminate(p, ctx['timeout'])
```

## psutil

```python
def terminate(process, timeout):
    name = process.name()

    def on_terminate(p):
        assert p.returncode == 0

    process.terminate()
    _, alive = psutil.wait_procs(
        [process], timeout=timeout, callback=on_terminate)
    if process in alive:
        process.kill()
        assert False, f'{name} (pid {process.pid}) killed'
```

## psutil

```python
>>> from socket import socket
>>> s = socket()
>>> s.bind(('', 0))
>>> s.listen(5)
```

## psutil

```python
>>> import psutil
>>> p = psutil.Process(17602)
>>> p.connections()
[pconn(fd=11,
  family=<AddressFamily.AF_INET: 2>,
  type=<SocketKind.SOCK_STREAM: 1>,
  laddr=addr(ip='0.0.0.0', port=56543),
  raddr=(),
  status='LISTEN')]
```


# Stdlib

## datetime.date

<small>

|Function|Description|
|--|--|
|date(year, month, day)|date object.|
|isocalendar(...)|Return a 3-tuple containing ISO year, week number, and weekday.|
|isoformat(...)|Return string in ISO 8601 format, YYYY-MM-DD.|
|isoweekday(...)|Return the day of the week.|
|fromisoformat(...)|Construct a date from the output of date.isoformat().|
|fromordinal(...)|date corresponding to a proleptic Gregorian ordinal.|
|fromtimestamp(...)|local date from a POSIX timestamp.|
|today(...)|Current date or datetime.|

</small>

## datetime.datetime

<small>

`datetime(year, month, day[, hour[, minute[, second[, microsecond[,tzinfo]]]]])`

</small>

## datetime.timedelta

<small>

`timedelta(days=0, seconds=0, microseconds=0, milliseconds=0, minutes=0, hours=0, weeks=0)`

</small>

## datetime.timedelta

<small>

|Operation|Result|
|--|--|
|t1 = t2 + t3|Sum of t2 and t3. Afterwards t1-t2 == t3 and t1-t3 == t2 are true.|
|t1 = t2 - t3|Difference of t2 and t3. Afterwards t1 == t2 - t3 and t2 == t1 + t3 are true.|
|t1 = t2 * i or t1 = i * t2|Delta multiplied by an integer. Afterwards t1 // i == t2 is true, provided i != 0.|
|t1 = t2 * f or t1 = f * t2|Delta multiplied by a float. The result is rounded to the nearest multiple of timedelta.resolution using round-half-to-even.|
|f = t2 / t3|Division of overall duration t2 by interval unit t3. Returns a float object.|

</small>

## datetime.timedelta

<small>

|Operation|Result|
|--|--|
|t1 = t2 / f or t1 = t2 / i|Delta divided by a float or an int. The result is rounded to the nearest multiple of timedelta.resolution using round-half-to-even.|
|t1 = t2 // i or t1 = t2 // t3|The floor is computed and the remainder (if any) is thrown away. In the second case, an integer is returned.|
|t1 = t2 % t3|The remainder is computed as a timedelta object.|
|+t1|Returns a timedelta object with the same value.|
|-t1|equivalent to timedelta(-t1.days, -t1.seconds, -t1.microseconds), and to t1* -1.|

</small>

## TASK

write an efficient algorithm to find median in a stream of running integers

<small>

@Wikipedia *median*: The median is the value separating the higher half from the lower half of a data sample.

</small>

::: notes
https://wandbox.org/permlink/3TG9WyvkkevpdzMG
:::

## heapq

<small>

This implementation uses arrays for which `heap[k] <= heap[2*k+1]` and
`heap[k] <= heap[2*k+2]` for all `k`, counting elements from zero.

|Function|Description|
|--|--|
|heapify(...)|Transform list into a heap, in-place, in O(len(heap)) time.|
|heappush(heap, item) -> None|Push item onto heap, maintaining the heap invariant.|
|heappop(...)|Pop the smallest item off the heap, maintaining the heap invariant.|

</small>

## weakref

```python
>>> class MyList(list):
...    pass
>>> l = MyList([1,2,3,4,5])
```

## weakref.ref

```python
>>> import weakref
>>> r = weakref.ref(l)
>>> r
<weakref at 0x7f95f0592098; to 'MyList' at 0x7f95f0592ef8>
>>> r[0]
TypeError
<ipython-input-7-8418cdc095ae> in <module>
----> 1 r[0]

TypeError: 'weakref' object is not subscriptable
>>> r()
[1, 2, 3, 4, 5]
```

::: notes
The ref constructor accepts an optional callback function that is invoked when the referenced object is deleted.
:::

## weakref.proxy

```python
>>> p = weakref.proxy(l)
>>> p
[1, 2, 3, 4, 5]
>>> p[0]
1
>>> type(p)
weakproxy
>>> p()
TypeError
<ipython-input-12-d40139f363ae> in <module>
----> 1 p()

TypeError: 'weakproxy' object is not callable
```

## copy

<small>

|Function|Description|
|--|--|
|copy(x)|Shallow copy operation on arbitrary Python objects.|
|deepcopy(x, memo=None, _nil=[])|Deep copy operation on arbitrary Python objects.|

</small>

::: notes
`memo` helps to solve the problem of deepcoping objects
with recursive references
:::

## hashlib

```python
>>> import hashlib
>>> ' '.join(hashlib.algorithms_guaranteed)
'sha384 blake2s sha3_256 sha3_512 sha3_224 sha3_384 md5 sha224 sha256 shake_128 sha1 shake_256 blake2b sha512'
```

## hashlib

<small>

|Function|Description|
|--|--|
|update(...)|Update this hash object's state with the provided string.|
|digest(...)|Return the digest value as a bytes object.|
|hexdigest(...)|Return the digest value as a string of hexadecimal digits.|
|copy(...)|Return a copy of the hash object.|

</small>

## hashlib

```python
>>> import hashlib
>>> h = hashlib.new('sha1')
>>> h.update(b'lorem')
>>> h.hexdigest()
'b58e92fff5246645f772bfe7a60272f356c0151a'
>>> h.update(b'ipsum')
>>> In [24]: h.hexdigest()
'7634a6ca6b194665b6dc5b3b8a6f44f12e299dc7'
```
```python
>>> g = hashlib.new('sha1')
>>> g.update(b'ipsum')
>>> g.hexdigest()
'da3ba44badb2f8e556b72781c425657067c037e6'
```

## hashlib

TASK: find duplicates in directory by hashing

## binascii

<small>

|Function|Description|
|--|--|
|hexlify(data)|Hexadecimal representation of binary data.|
|unhexlify(hexstr)|Binary data of hexadecimal representation.|

</small>

# Networking

## socket

```python
>>> import socket
>>> server = socket.socket(family=socket.AF_INET,
...                        type=socket.SOCK_STREAM)
>>> server.bind(('', 0))
>>> server.getsockname()
('0.0.0.0', 42979)
>>> server.listen(5)
>>> client, addr = server.accept()
```

## socket

```python
>>> import socket
>>> client = socket.socket(family=socket.AF_INET,
...                        type=socket.SOCK_STREAM)
>>> client.connect(('0.0.0.0', 42979))
```
```python
>>> client, addr = server.accept()
>>> addr
('127.0.0.1', 42838)
>>> client.recv(1024)
```

## socket

```python
>>> client.send(b'hello')
5
```
```python
>>> client.recv(1024)
>>> b'hello'
```

## select

```python
import select
import socket
import sys
from queue import Queue, Empty
import logging
logging.basicConfig(
    format='%(process)d [%(threadName)s] $ %(message)s',
    level=logging.INFO)
log = logging.getLogger(__name__)

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.setblocking(0)
server.bind(('', 8888))
server.listen(5)
```

## select

```python
inputs = [server]
outputs = []
message_queues = {}
```

## select

```python
log.info('entering loop...')
while inputs:
    readable, writable, exceptional = select.select(
        inputs, outputs, inputs)
```

## select

```python
for s in readable:
    if s is server:
        con, (host, port) = s.accept()
        log.info(f'connection from {host!r}:{port} accepted')
        con.setblocking(0)
        inputs.append(con)
        message_queues[con] = Queue()
```

## select

```python
else:
    data = s.recv(1024)
    if data:
        host, port = s.getpeername()
        log.info(f'received {data!r} from {host!r}:{port}')
        message_queues[s].put(data)
        if s not in outputs:
            outputs.append(s)
    else:
        if s in outputs:
            outputs.remove(s)
        inputs.remove(s)
        s.close()
        del message_queues[s]
```

## select

```python
for s in writable:
    try:
        msg = message_queues[s].get_nowait()
    except Empty:
        outputs.remove(s)
    else:
        host, port = s.getpeername()
        log.info(f'sending message {msg!r} to {host!r}:{port}')
        s.send(msg)
```

## select

```python
for s in exceptional:
    inputs.remove(s)
    if s in outputs:
        outputs.remove(s)
    s.close()
    del message_queues[s]
```

## select

```python
>>> import time
>>> import socket
>>> client = socket.socket()
>>> client.connect(('localhost', 8888))
>>> for i in range(100):
...   client.send(b'lorem')
...   client.recv(1024)
...   time.sleep(0.3)
...
5
b'lorem'
```

## select

```python
>>> import time
>>> import socket
>>> client = socket.socket()
>>> client.connect(('localhost', 8888))
>>> for i in range(100):
...   client.send(b'ipsum')
...   client.recv(1024)
...   time.sleep(0.1)
...
5
b'ipsum'
```

## select

[code sample](https://gitlab.com/attu/python_intro/blob/master/examples/select/simple.py)

<small>

```text
[15727:MainThread] $ entering loop...                                           
[15727:MainThread] $ connection from '127.0.0.1':46338 accepted
[15727:MainThread] $ received b'lorem' from '127.0.0.1':46338
[15727:MainThread] $ sending message b'lorem' to '127.0.0.1':46338
[15727:MainThread] $ received b'lorem' from '127.0.0.1':46338
[15727:MainThread] $ sending message b'lorem' to '127.0.0.1':46338
[15727:MainThread] $ received b'lorem' from '127.0.0.1':46338
[15727:MainThread] $ sending message b'lorem' to '127.0.0.1':46338
[15727:MainThread] $ connection from '127.0.0.1':46340 accepted
[15727:MainThread] $ received b'ipsum' from '127.0.0.1':46340
[15727:MainThread] $ sending message b'ipsum' to '127.0.0.1':46340
[15727:MainThread] $ received b'ipsum' from '127.0.0.1':46340
[15727:MainThread] $ sending message b'ipsum' to '127.0.0.1':46340
[15727:MainThread] $ received b'ipsum' from '127.0.0.1':46340
[15727:MainThread] $ sending message b'ipsum' to '127.0.0.1':46340
[15727:MainThread] $ received b'lorem' from '127.0.0.1':46338
[15727:MainThread] $ sending message b'lorem' to '127.0.0.1':46338
```

</small>

## pyzmq

[source code](https://gitlab.com/attu/python_intro/tree/master/examples/pyzmq)

```text
$ tree
.
├── dcomp
│   ├── broker.py
│   ├── client.py
│   ├── console.py
│   ├── __init__.py
│   ├── isprime.py
│   ├── sink.py
│   └── worker.py
├── docker-compose.yml
├── htpasswd
├── pypirc
├── setup.py
└── upload
```

## pyzmq: setup.py

```python
from setuptools import setup, find_packages
setup(
    name='broker',
    version='0.1',
    packages=find_packages(),
    install_requires=['pyzmq>=18.0'],
    entry_points={
        'console_scripts': [
            'broker = dcomp.broker:main',
            'sink = dcomp.sink:main',
            'client = dcomp.client:main',
            'worker = dcomp.worker:main',]
    }
)
```

## pyzmq: broker

```python
def main():
    args = console()
    context = zmq.Context()
    frontend = context.socket(zmq.ROUTER)
    backend = context.socket(zmq.DEALER)
    frontend.bind(f'tcp://*:{args.broker_frontend_port}')
    backend.bind(f'tcp://*:{args.broker_backend_port}')
```

## pyzmq: broker

```python
    while True:
        socks = dict(poller.poll())

        if socks.get(frontend) == zmq.POLLIN:
            message = frontend.recv_multipart()
            backend.send_multipart(message)

        if socks.get(backend) == zmq.POLLIN:
            message = backend.recv_multipart()
            frontend.send_multipart(message)
```

## pyzmq: worker

```python
def main():
    args = console()

    context = zmq.Context()
    socket = context.socket(zmq.REP)
    sink = context.socket(zmq.PUSH)
    sink.connect(f'tcp://{args.sink_host}:{args.sink_port}')
    bq = f'tcp://{args.broker_host}:{args.broker_backend_port}'
    socket.connect(bq)

    uid = str(uuid4())
```

## pyzmq: worker

```python
    while True:
        msg = json.loads(socket.recv().decode('utf-8'))
        socket.send(b'OK')
        if check(msg['value']):
            msg['status'] = 'prime'
            msg['worker'] = uid
            sink.send(json.dumps(msg).encode('utf-8'))
```

## pyzmq: isprime

```python
def check(num):
    print(f'checking prime {num}')
    sleep(0.1)
    return False
```

## TASK

Replace dummy implementation of `isprime.check` with real primality test
and attach your worker to the pool.

## urllib3

::: notes
TODO
:::

## simple http server

```sh
python2 -m SimpleHTTPServer 8000
python3 -m http.server 8001
```

#  Concurrent computing

## GIL

`GIL` Global Interpeter Lock

process load:

- cpu-bound
- io-bound

## concurrency

||no data sharing|data sharing|
|--|--|--|
|mutable data|no synchronization|**synchronization**|
|immutable data|no synchronization|no synchronization|

## concurrency

<small>

|Type|Scheduling|Unit|Description|Library|
|--|--|--|--|--|
|pre-emptive|external|thread|single-core|`threading`|
|cooperative|internal|task|single-core|`asyncio`|
|multiprocessing|external|process|multi-core|`multiprocessing`|

</small>

## threading

```python
def job():
    log.info('starting job')
    time.sleep(3)
    log.info('job done')
```

## threading

```python
log.info('creating thread')
t = threading.Thread(target=job)
log.info(f'starting thread {t!r}')
t.start()
t.join()
log.info(f'joined {t!r}')
```

## threading

[code sample](https://gitlab.com/attu/python_intro/blob/master/examples/threading/simple.py)

```text
[MainThread] $ creating thread
[MainThread] $ starting thread <Thread(Thread-1, initial)>
[Thread-1] $ starting job
[Thread-1] $ job done
[MainThread] $ joined <Thread(Thread-1, stopped 139652778665728)>
```

## threading: Lock

```python
class Counter:

    def __init__(self):
        self.lock = Lock()
        self.value = 0


def worker(cnt: Counter):
    log.info('waiting for lock')
    time.sleep(randrange(1, 3))
    with cnt.lock:
        log.info(f'lock acquired, value: {cnt.value}')
        cnt.value += 1
```

## threading: Lock

[code sample](https://gitlab.com/attu/python_intro/blob/master/examples/threading/lock.py)

```text
[5880:Thread-1] $ waiting for lock
[5880:Thread-2] $ waiting for lock
[5880:Thread-3] $ waiting for lock
[5880:Thread-4] $ waiting for lock
[5880:Thread-2] $ lock acquired, value: 0
[5880:Thread-3] $ lock acquired, value: 1
[5880:Thread-1] $ lock acquired, value: 2
[5880:Thread-4] $ lock acquired, value: 3
```

## threading: Lock

<small>

|Function|Description|
|--|--|
|acquire(...)|Lock the lock. Blocks until the lock is released.|
|release(...)|Release the lock, allowing blocked thread to acquire the lock.|
|locked(...)->bool|Return whether the lock is in the locked state.|
|`__enter__(...)`|Context manager enter: acquire.|
|`__exit__(...)`|Context manager exit: release.|

</small>

## threading: Condition

```python
def consumer(cond: Condition):
    log.info('starting consumer')
    with cond:
        cond.wait()
        log.info('resource is available')
        cond.notify(n=1)
        log.info('consumer notify')


def producer(cond: Condition):
    log.info('starting producer')
    with cond:
        log.info('creating resource')
        cond.notify(n=1)
```

## threading: Condition

```python
cond = Condition()
cons = [Thread(target=consumer, args=(cond,)) for _ in range(2)]
prod = Thread(target=producer, args=(cond,))

for c in cons:
    c.start()
    time.sleep(0.1)
prod.start()
```

## threading: Condition

[code sample](https://gitlab.com/attu/python_intro/blob/master/examples/threading/condition_variable.py)

```text
[2333:Thread-1] $ starting consumer
[2333:Thread-2] $ starting consumer
[2333:Thread-3] $ starting producer
[2333:Thread-3] $ creating resource
[2333:Thread-1] $ resource is available
[2333:Thread-1] $ consumer notify
[2333:Thread-2] $ resource is available
[2333:Thread-2] $ consumer notify
```

## threading: Condition

<small>

|Function|Description|
|--|--|
|notify(self, n=1)|Wake up one or more threads waiting on this condition, if any.|
|notify_all(self)|Wake up all threads waiting on this condition.|
|wait(self, timeout=None)|Wait until notified or until a timeout occurs.|
|wait_for(self, predicate, timeout=None)|Wait until a condition evaluates to True.|
|`__enter__`|Context manager enter.|
|`__exit__`|Context manager exit.|
</small>

## threading

`Semaphore`, `Event`, `Barrier`, `Timer`, `threading.local`

## multiprocessing

```python
def job():
    log.info('starting job')
    time.sleep(3)
    log.info('job done')


log.info('creating process')
p = multiprocessing.Process(target=job)
log.info(f'starting process {p!r}')
p.start()
p.join()
log.info(f'joined {p!r}')
```

## multiprocessing

[code sample](https://gitlab.com/attu/python_intro/blob/master/examples/multiprocessing/simple.py)

```text
[9871:MainThread] $ creating process
[9871:MainThread] $ starting process <Process(Process-1, initial)>
[9872:MainThread] $ starting job
[9872:MainThread] $ job done
[9871:MainThread] $ joined <Process(Process-1, stopped)>
```

## multiprocessing

<small>

|Function|Description|
|--|--|
|is_alive(self)|Return whether thread/process is alive.|
|join(self, timeout=None)|Wait until thread/process terminates.|
|run(self)|Method to be run in thread/sub-process.|
|start(self)|Start thread/process.|

</small>

::: notes
`multiprocessing.Process` mimics `threading.Thread` api so that
it can be a drop-in replacement
:::

## multiprocessing

<small>

|Function|Description|
|--|--|
|close(self)|Close the Process object.|
|terminate(self)|Terminate process; sends SIGTERM.|
|kill(self)|Terminate process; sends SIGKILL.|

</small>

## multiprocessing

```python
def job():
    time.sleep(random.randrange(1, 3))
    return random.randrange(10, 20)
```

## multiprocessing

```python
class Consumer(Process):

    def __init__(self, tasks: JoinableQueue, results: Queue):
        Process.__init__(self)
        self.tasks = tasks
        self.results = results
```

## multiprocessing

```python
def run(self):
    while True:
        next_task = self.tasks.get()
        if next_task is None:
            log.info(f'{self.name}: Exiting')
            self.tasks.task_done()
            break
        log.info(f'{self.name} is doing task')
        answer = next_task()
        self.tasks.task_done()
        self.results.put(answer)
```

## multiprocessing

```python
tasks = JoinableQueue()
results = Queue()

consumers = [Consumer(tasks, results) for i in range(3)]

for w in consumers:
    w.start()

num_jobs = 6
for i in range(num_jobs):
    tasks.put(job)
```

## multiprocessing

```python
for c in consumers:
    tasks.put(None)

tasks.join()

for _ in range(num_jobs):
    log.info(f'Result is {results.get()}')
```

## multiprocessing

[code sample](https://gitlab.com/attu/python_intro/blob/master/examples/multiprocessing/queue_task.py)

```text
[9246:MainThread] $ Consumer-1 is doing task
[9247:MainThread] $ Consumer-2 is doing task
[9248:MainThread] $ Consumer-3 is doing task
[9246:MainThread] $ Consumer-1 is doing task
[9247:MainThread] $ Consumer-2 is doing task
[9248:MainThread] $ Consumer-3 is doing task
[9246:MainThread] $ Consumer-1: Exiting
[9247:MainThread] $ Consumer-2: Exiting
[9248:MainThread] $ Consumer-3: Exiting
[9245:MainThread] $ Result is 17
[9245:MainThread] $ Result is 17
[9245:MainThread] $ Result is 13
[9245:MainThread] $ Result is 17
[9245:MainThread] $ Result is 19
[9245:MainThread] $ Result is 16
```

## multiprocessing: Queue

<small>

|Function|Description|
|--|--|
|Queue([maxsize])|Returns a process shared queue implemented.|
|empty(self)|Return True if the queue is empty, False otherwise.|
|full(self)|Return True if the queue is full, False otherwise.|
|qsize(self)|Return the approximate size of the queue.|

</small>

::: notes
values obtained by above getters are not reliable due to the nature
of multiprocessing
:::

## multiprocessing: Queue

<small>

|Function|Description|
|--|--|
|close(self)|Indicate that no more data will be put on this queue by the current process.|
|get(self, block=True, timeout=None)|Put obj into the queue. Block if necessary until a free slot is available.|
|put(self, obj, block=True, timeout=None)|Remove and return an item from the queue. Block if necessary until an item is available.|

</small>

::: notes
get does both front + pop so that both operation can be atomic
:::

## multiprocessing: JoinableQueue

<small>

|Function|Description|
|--|--|
|join(self)|Block until all items in the queue have been gotten and processed.|
|task_done(self)|Indicate that a formerly enqueued task is complete.|

</small>

## multiprocessing: signaling

`Even`, `Semaphore`, `Lock`, `Condition`

## multiprocessing: shared state

```python
def producer(ns, event):
    ns.value = 'lorem'
    log.info(f'namespace id#{id(ns)} value set to {ns.value!r}')
    event.set()


def consumer(ns, event):
    log.info('awaiting event')
    event.wait()
    log.info(f'got event, ns id#{id(ns)} value is {ns.value!r}')
```

## multiprocessing: shared state

```python
mgr = Manager()
namespace = mgr.Namespace()
event = Event()
p = Process(target=producer, args=(namespace, event))
c = Process(target=consumer, args=(namespace, event))

c.start()
p.start()
```

## multiprocessing: shared state

[code sample](https://gitlab.com/attu/python_intro/blob/master/examples/multiprocessing/shared_state.py)

```text
[29580:MainThread] $ awaiting event
[29581:MainThread] $ namespace id#139919074685560 value set to 'lorem'
[29580:MainThread] $ got event, ns id#139919074685560 value is 'lorem'
```

## concurrent.futures

```python
from concurent.futures import ThreadPoolExecutor as Executor
import shutil

with Executor(max_workers=4) as pool:
    pool.submit(shutil.copy, 'src1.txt', 'dest1.txt')
    pool.submit(shutil.copy, 'src2.txt', 'dest2.txt')
    pool.submit(shutil.copy, 'src3.txt', 'dest3.txt')
    pool.submit(shutil.copy, 'src4.txt', 'dest4.txt')
```

## concurrent.futures

[`Executor`](https://docs.python.org/3/library/concurrent.futures.html#concurrent.futures.Executor)

<small>

|Function|Description|
|--|--|
|submit(fn, \*args, **kwargs)|Schedules the callable, fn, to be executed as fn(\*args, **kwargs).|
|map(func, \*iterables, ...)|Equivalent to map(func, \*iterables) except func is executed asynchronously and several calls to func may be made concurrently.|
|shutdown(wait=True)|Signal the executor that it should free any resources that it is using when the currently pending futures are done executing.|

</small>

## concurrent.futures

Executors:

- ThreadPoolExecutor
- ProcessPoolExecutor

## concurrent.futures

[`Future`](https://docs.python.org/3.7/library/concurrent.futures.html#future-objects)

<small>

|Function|Description|
|--|--|
|cancel()|Attempt to cancel the call.|
|cancelled()|Return True if the call was successfully cancelled.|
|running()|Return True if the call is currently being executed and cannot be cancelled.|
|done()|Return True if the call was successfully cancelled or finished running.|
|result(timeout=None)|Return the value returned by the call.|
|add_done_callback(fn)|Attaches the callable fn to the future.|

</small>

## asyncio

::: notes
The asyncio module provides tools for building concurrent applications using coroutines.
While the threading module implements concurrency through application threads
and multiprocessing implements concurrency using system processes,
asyncio uses a single-threaded, single-process approach in which parts of
an application cooperate to switch tasks explicitly at optimal times
:::

```python
import asyncio


async def outer():
    log.info(f'calling coro_1')
    a = await coro_1()
    log.info(f'calling coro_2')
    b = await coro_2(a)
    log.info(f'returning outer')
    return a, b
```

## asyncio

```python
async def coro_1():
    log.info(f'coro_1 called')
    return 42


async def coro_2(x):
    log.info(f'coro_2({x}) called')
    return 'lorem'
```

## asyncio

```python
loop = asyncio.get_event_loop()

try:
    result = loop.run_until_complete(outer())
    log.info(f'outer returned {result}')
finally:
    loop.close()
```

## asyncio

[code sample](https://gitlab.com/attu/python_intro/blob/master/examples/asyncio/simple.py)

```text
[MainThread] $ calling coro_1
[MainThread] $ coro_1 called
[MainThread] $ calling coro_2
[MainThread] $ coro_2(42) called
[MainThread] $ returning outer
[MainThread] $ outer returned (42, 'lorem')
```

## asyncio: streams

```python
async def main():
    server = await asyncio.start_server(
        handle_connection, '0.0.0.0')
    host, port = server.sockets[0].getsockname()
    log.info(f'stream server starts on address {host!r}:{port}')
    async with server:
        await server.serve_forever()
    await asyncio.gather(*asyncio.all_tasks())


with suppress(KeyboardInterrupt):
    asyncio.run(main())
```

::: notes
Blocking function calls must be avoided
when using asyncio. Most of important python
blocking functions have awaitable wrappers.
:::

## asyncio: streams

```python
async def handle_connection(reader: StreamReader,
                            writer: StreamWriter):
    host, port = writer.get_extra_info('peername')
    log.info(f'connecting {host!r}:{port}')
    while True:
        message = await reader.read(1024)
        if not message or message == b'exit':
            break
        message = message.decode().strip()
        ret = await handle_message(message, writer, host, port)
        await writer.drain()
    log.info(f'{host!r}:{port} connection closed')
    writer.close()
```

## asyncio: streams

```python
async def handle_message(message: str,
                         writer: StreamWriter,
                         host: str = 'localhost',
                         port: int = 0):
    log.info(f'{host!r}:{port} sends {message!r}')
    asyncio.create_task(job(message, writer))
    log.info(f'message {message!r} handled')


async def job(message: str, writer: StreamWriter):
    log.info(f'starting task {message!r}')
    await asyncio.sleep(uniform(0.1, 0.5))
    log.info(f'task {message!r} finished')
    writer.write(b'done')
```

## asyncio: streams

[code sample](https://gitlab.com/attu/python_intro/blob/master/examples/asyncio/streams.py)

<small>

```text
[14722:MainThread] $ stream server starts on address '0.0.0.0':42389            
[14722:MainThread] $ connecting '127.0.0.1':54132
[14722:MainThread] $ connecting '127.0.0.1':54134
[14722:MainThread] $ connecting '127.0.0.1':54136
[14722:MainThread] $ '127.0.0.1':54132 sends 'dolor'
[14722:MainThread] $ message 'dolor' handled
[14722:MainThread] $ '127.0.0.1':54134 sends 'lorem'
[14722:MainThread] $ message 'lorem' handled
[14722:MainThread] $ starting task 'dolor'
[14722:MainThread] $ starting task 'lorem'
[14722:MainThread] $ '127.0.0.1':54136 sends 'sit'
[14722:MainThread] $ message 'sit' handled
[14722:MainThread] $ starting task 'sit'
[14722:MainThread] $ task 'lorem' finished
[14722:MainThread] $ '127.0.0.1':54134 sends 'ipsum'
[14722:MainThread] $ message 'ipsum' handled
[14722:MainThread] $ starting task 'ipsum'
[14722:MainThread] $ task 'sit' finished
[14722:MainThread] $ '127.0.0.1':54136 connection closed
[14722:MainThread] $ task 'dolor' finished
[14722:MainThread] $ '127.0.0.1':54132 connection closed
[14722:MainThread] $ task 'ipsum' finished
[14722:MainThread] $ '127.0.0.1':54134 connection closed
```

</small>

## asyncio: blocking

```python
async def handle_message(pool: Executor,
                         message: str,
                         writer: StreamWriter,
                         host: str = 'localhost',
                         port: int = 0):
    log.info(f'{host!r}:{port} sends {message!r}')
    asyncio.create_task(job(message, writer))
    loop = asyncio.get_event_loop()
    blocking_tasks = [loop.run_in_executor(
        pool, blocking, i, message) for i in range(3, 5)]
    completed, pending = await asyncio.wait(blocking_tasks)
    log.info(f'message {message!r} handled')
```

::: notes
When call to a blocking functions is necessary
it can be awaited on executor.
:::

## asyncio: blocking

[code sample](https://gitlab.com/attu/python_intro/blob/master/examples/asyncio/blocking.py)

```python
if sys.argv[1] == 'process':
    from concurrent.futures import ProcessPoolExecutor as Executor

if sys.argv[1] == 'thread':
    from concurrent.futures import ThreadPoolExecutor as Executor
```

## asyncio: blocking

<small>

```text
$ python examples/asyncio/blocking.py thread                                    
[11454:MainThread] $ stream server starts on address '0.0.0.0':38463
[11454:MainThread] $ connecting '127.0.0.1':42910
[11454:MainThread] $ connecting '127.0.0.1':42912
[11454:MainThread] $ '127.0.0.1':42912 sends 'dolor'
[11454:ThreadPoolExecutor-0_0] $ I am blocking 'dolor'
[11454:ThreadPoolExecutor-0_1] $ I am blocking 'dolor'
[11454:MainThread] $ '127.0.0.1':42910 sends 'lorem'
[11454:ThreadPoolExecutor-0_2] $ I am blocking 'lorem'
[11454:MainThread] $ connecting '127.0.0.1':42914
[11454:MainThread] $ starting task 'dolor'
[11454:MainThread] $ starting task 'lorem'
[11454:MainThread] $ '127.0.0.1':42914 sends 'sit'
[11454:MainThread] $ starting task 'sit'
[11454:MainThread] $ task 'lorem' finished
[11454:MainThread] $ task 'sit' finished
[11454:MainThread] $ task 'dolor' finished
[11454:ThreadPoolExecutor-0_2] $ 'lorem' done...
[11454:ThreadPoolExecutor-0_2] $ I am blocking 'lorem'
```

</small>

## asyncio: blocking

<small>

```text
$ python examples/asyncio/blocking.py process                                   
[11491:MainThread] $ stream server starts on address '0.0.0.0':42449
[11491:MainThread] $ connecting '127.0.0.1':42270
[11491:MainThread] $ connecting '127.0.0.1':42272
[11491:MainThread] $ connecting '127.0.0.1':42274
[11491:MainThread] $ '127.0.0.1':42270 sends 'lorem'
[11491:MainThread] $ '127.0.0.1':42274 sends 'sit'
[11495:MainThread] $ I am blocking 'lorem'
[11491:MainThread] $ starting task 'lorem'
[11497:MainThread] $ I am blocking 'sit'
[11496:MainThread] $ I am blocking 'lorem'
[11491:MainThread] $ starting task 'sit'
[11491:MainThread] $ '127.0.0.1':42272 sends 'dolor'
[11491:MainThread] $ starting task 'dolor'
[11491:MainThread] $ task 'lorem' finished
[11491:MainThread] $ task 'sit' finished
[11491:MainThread] $ task 'dolor' finished
[11497:MainThread] $ 'sit' done...
[11497:MainThread] $ I am blocking 'sit'
[11495:MainThread] $ 'lorem' done...
[11495:MainThread] $ I am blocking 'dolor'
[11496:MainThread] $ 'lorem' done...
[11496:MainThread] $ I am blocking 'dolor'
```

</small>

# Tools

## ipython

A powerful interactive shell.

::: notes
kernel of Jupyter
:::

## python-jedi

[project page](https://jedi.readthedocs.io/en/latest/)

Smart completions and static analysis.

- vim plugin
- atom plugin
- etc..

## pdb

::: notes
TODO
:::

## autopep8

[proposal](https://www.python.org/dev/peps/pep-0008/)

[pep8](https://pypi.org/project/pep8/)

## autopep8

<small>

|error code|description|
|--|--|
|E1| Indentation |
|E2| Whitespace |
|E3| Blank line |
|E4| Import |
|E5| Line length |
|E7| Statement |
|E9| Runtime |

</small>

## autopep8

<small>

|error code|description|
|--|--|
|E101| indentation contains mixed spaces and tabs |
|E111| indentation is not a multiple of four |
|E221| multiple spaces before operator |
|E222| multiple spaces after operator |
|E303| too many blank lines |
|E402| module level import not at top of file |
|E501| line too long |
|E702| multiple statements on one line |
|E731| do not assign a lambda expression, use a def |

</small>

## autopep8

[autopep8](https://pypi.org/project/autopep8/)

```text
$ autopep8 -iaa ./myscript.py
```

## autopep8

<small>

|option|description|
|--|--|
|-d, --diff| print the diff for the fixed source |
|-i, --in-place| make changes to files in place |
|-r, --recursive| run recursively over directories |
|-a, --aggressive| enable non-whitespace changes; multiple -a result in more aggressive changes |
|--ignore errors| do not fix these errors/warnings |
|--select errors| fix only these errors/warnings |
|--max-line-length| set maximum allowed line length |
|--line-range| only fix errors found within this inclusive range of line numbers |

</small>

## autopep8

```text
$ cat setup.cfg
[pep8]
ignore = E226,E302,E41
max-line-length = 160
```

## sphinx

::: notes
TODO
:::

## pytest

::: notes
TODO
:::

## behave

::: notes
TODO
:::

## tox

::: notes
TODO
:::

## pypiserver

[homepage](https://pypi.org/project/pypiserver/)

`pypiserver` is a minimal PyPI compatible server for pip or easy_install.

## pypiserver: docker-compose.yml

```yaml
services:
  pypiserver:
    command: -P /data/auth/.htpasswd -a update /data/packages
    container_name: pypiserver
    image: pypiserver/pypiserver
    ports:
    - published: 8080
      target: 8080
    volumes:
    - packages:/data/packages:rw
    - ./htpasswd:/data/auth/.htpasswd:rw
version: '3.7'
volumes:
  packages: {}
```

## pypiserver: pypirc

```config
[distutils]
index-servers =
    devpi

[devpi]
repository: http://localhost:8080
username:devpi
```

## pypiserver: upload

```sh
$ python setup.py sdist upload -r devpi
```

## pypiserver: install

```sh
pip install --extra-index-url http://localhost:8080/ PACKAGE
```

# Credits

## useful links

- [1] <https://www.python.org/>
- [2] _Learning Python_, 5th Edition by Mark Lutz
- [3] [Python 3 Module of the Week](https://pymotw.com)
- [4] <https://realpython.com>

# QA
